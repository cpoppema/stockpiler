<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php print ApplicationName; ?> - <?php print $title; ?></title>
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/basic.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.responsive.css" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php print option('base_uri'); ?>public/img/logo.ico">
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/jquery.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/common.js"></script>
</head>
<body>
    <div class="container">
        <h1><?php print ApplicationName?></h1>
        <?php if ($_GET['error'] != null) { ?>
        <div class="alert alert-error">
            <strong>Error:</strong> <?php print $_GET['error']; ?>
        </div>
        <?php } ?>
        <?php if ($_GET['warning'] != null) { ?>
        <div class="alert alert-warning">
            <strong>Warning:</strong> <?php print $_GET['warning']; ?>
        </div>
        <?php } ?>
        <?php if ($_GET['success'] != null) { ?>
        <div class="alert alert-success">
            <strong>Success:</strong> <?php print $_GET['success']; ?>
        </div>
        <?php } ?>
        <?php if ($_GET['info'] != null) { ?>
        <div class="alert alert-info">
            <strong>Information:</strong> <?php print $_GET['info']; ?>
        </div>
        <?php } ?>
        <?php print $content; ?>
        <footer>
            <p><a href="<?php print option('base_uri'); ?>"><?php print ApplicationName; ?></a> is powered by <a href="http://github.com/mbmccormick/limoncello" target="_blank">Limoncello</a>. Version <a href="<?php print url_for('history'); ?>"><?php print Version; ?></a>.</p>
        </footer>
    </div>
    </body>
</html>

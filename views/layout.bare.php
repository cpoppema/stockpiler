<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php print ApplicationName; ?> - <?php print $title; ?></title>
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/layout.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.responsive.css" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php print option('base_uri'); ?>public/img/logo.ico">
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/jquery.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/common.js"></script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="<?php print option('base_uri'); ?>"><?php print ApplicationName; ?></a>
                <div class="nav-collapse">
					<ul class="nav">
						<li class="active"><a href="javascript:void(0);">Stock for products</a></li>
					</ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content">
            <div class="page-header">
                <h2><?php print $title?></h2>
            </div>
			<?php if ($_GET['error'] != null) { ?>
			<div class="alert alert-error">
				<strong>Error:</strong> <?php print $_GET['error']; ?>
			</div>
			<?php } ?>
			<?php if ($_GET['warning'] != null) { ?>
			<div class="alert alert-warning">
				<strong>Warning:</strong> <?php print $_GET['warning']; ?>
			</div>
			<?php } ?>
			<?php if ($_GET['success'] != null) { ?>
			<div class="alert alert-success">
				<strong>Success:</strong> <?php print $_GET['success']; ?>
			</div>
			<?php } ?>
			<?php if ($_GET['info'] != null) { ?>
			<div class="alert alert-info">
				<strong>Information:</strong> <?php print $_GET['info']; ?>
			</div>
			<?php } ?>
			<?php print $content; ?>
        </div>
    </div>
    </body>
</html>

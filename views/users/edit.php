<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>users/<?php print $user['id']; ?>/edit" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input class="input-xlarge" id="username" name="username" type="text" value="<?php print $user['username']; ?>" <?php if ($_SESSION["CurrentUser_IsAdministrator"] != "1") { ?>disabled="true"<?php } ?> />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" value="<?php print $user['name']; ?>" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="email">Email Address</label>
                    <div class="controls">
                        <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                            <input class="input-xlarge email" id="email" name="email" type="text" value="<?php print $user['email']; ?>" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="newpassword">New Password</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input class="input-xlarge exclude" id="newpassword" name="newpassword" type="password" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="newpasswordconfirm">Confirm New Password</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input class="input-xlarge exclude" id="newpasswordconfirm" name="newpasswordconfirm" type="password" />
                        </div>
                    </div>
                </div>
                <br />
				<?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
					<?php if ($_SESSION["CurrentUser_IsAdministrator"] == "1") { ?>
						<div class="control-group">
							<div class="controls">
								<label class="checkbox">
									<input type="checkbox" name="isadministrator" value="1" <?php if ($_SESSION["CurrentUser_IsAdministrator"] != "1") { ?>class="uneditable-input"<?php } ?> <?php if ($user['isadministrator'] == 1) { ?>checked="true"<?php } ?> <?php if ($_SESSION["CurrentUser_IsAdministrator"] != "1") { ?>disabled="true"<?php } ?> /> This user is an administrator
								</label>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<label class="checkbox">
									<input type="checkbox" name="isreadonly" value="1" <?php if ($_SESSION["CurrentUser_IsAdministrator"] != "1") { ?>class="uneditable-input"<?php } ?> <?php if ($user['isreadonly'] == 1) { ?>checked="true"<?php } ?> <?php if ($_SESSION["CurrentUser_IsAdministrator"] != "1") { ?>disabled="true"<?php } ?> /> Read only access
								</label>
							</div>
						</div>
						<br />
					<?php } ?>
					<div class="control-group">
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" name="receivewarningemails" value="1" <?php if ($user['receivewarningemails'] == 1) { ?>checked="true"<?php } ?> /> Receive warning e-mails
							</label>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" name="bulkisdefault" value="1" <?php if ($user['bulkisdefault'] == 1) { ?>checked="true"<?php } ?> /> Bulk delivery and pickup by default
							</label>
						</div>
					</div>
                    <div class="control-group">
                        <div class="controls">
                            <label class="checkbox">
                                <input type="checkbox" name="manualmodeisdefault" value="1" <?php if ($user['manualmodeisdefault'] == 1) { ?>checked="true"<?php } ?> /> Manual mode instead of hand scanner mode in delivery and pickup by default
                            </label>
                        </div>
                    </div>
				<?php } ?>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="hideinactive" value="1" <?php if ($user['hideinactive'] == 1) { ?>checked="true"<?php } ?> /> Hide canceled orders, stock changes and deleted products by default
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="hidepagedescription" value="1" <?php if ($user['hidepagedescription'] == 1) { ?>checked="true"<?php } ?> /> Hide page descriptions and alike
                        </label>
                    </div>
                </div>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save User</button>&nbsp;<?php if ($user['isdeleted'] == 1 ) { ?><a onclick="return confirm('Are you sure you want to recover this user?');" href="<?php print option('base_uri'); ?>users/<?php print $user['id']; ?>/recover" class="btn">Recover user</a><?php } else { ?><a onclick="return confirm('Are you sure you want to delete this user?');" href="<?php print option('base_uri'); ?>users/<?php print $user['id']; ?>/delete" class="btn">Delete</a><?php } ?>
                <a href="<?php print url_for('users'); ?>" class="btn pull-right">Back</a>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to edit a user's information. You also have the ability to delete a user from this page.</p>
            <br />
            <h5>Change Password</h5>
            <p>You can change the password for this user by entering their existing password and then providing a new password for the user.</p>
            <br />
        </div>
    <?php } ?>
</div>
<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
		<?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
			<div class="well">
				<a href="<?php print option('base_uri'); ?>users/add" class="btn btn-primary">New User</a>
			</div>
		<?php } ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php print $body; ?>
            </tbody>
        </table>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page shows the list of users currently setup for the application. This list allows you to view a user's information or create a new user.</p>
            <br />
        </div>
    <?php } ?>
</div>
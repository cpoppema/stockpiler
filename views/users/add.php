<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>users/add" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input class="input-xlarge" id="username" name="username" type="text" autofocus="autofocus" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="email">Email Address</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input class="input-xlarge email" id="email" name="email" type="text" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="password">Password</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input class="input-xlarge" id="password" name="password" type="password" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="passwordconfirm">Confirm Password</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input class="input-xlarge" id="passwordconfirm" name="passwordconfirm" type="password" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="isadministrator" value="1" /> This user is an administrator
                        </label>
                    </div>
                </div>
				<div class="control-group">
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="isreadonly" value="1" /> Read only access
						</label>
					</div>
				</div>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add User</button>&nbsp;<button type="reset" class="btn">Cancel</button>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to add a new user to the application. Make sure that the email address you provide is valid, as it will be used to reset the user's password.</p>
            <br />
        </div>
    <?php } ?>
</div>
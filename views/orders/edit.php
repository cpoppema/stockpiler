<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>orders/<?php print $order['id']; ?>/edit" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="name">Product</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" value="<?php print $order['product']['name']; ?>" disabled="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="amount">Amount</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                            <input class="input-small" id="amount" name="amount" type="number" pattern="\d+" min="1" step="1" value="<?php print $order['amount']; ?>" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="hasarrived" value="1" <?php if ($order['hasarrived'] == 1) { ?>checked="true"<?php } ?>  <?php if ($order['hasarrived'] == 1) { ?>disabled="disabled"<?php } ?> /> Products have arrived.
                        </label>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="iscanceled" value="1" <?php if ($order['iscanceled'] == 1) { ?>checked="true"<?php } ?> /> This order has been canceled.
                        </label>
                    </div>
                </div>
                <hr />
                Record of deliveries:
                <?php if (!$has_all_products) { ?>
                    <a href="#" id="add-partial-delivery">Record a partial delivery</a>
                    <br />
                    <br />

                    <div id="add-partial-amount" class="well well-small hidden">
                        <div class="control-group pull-left">
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                                    <input class="input-small" id="partial-amount" name="partial-amount" type="number" pattern="\d+" min="1" <?php print "max='{$max_partial_amount}'"; ?> step="1" value="1" disabled="disabled" />
                                </div>
                            </div>
                        </div>
                        &nbsp;&nbsp;&nbsp;<button type="submit" name="partial-delivery" class="btn btn-primary">Record</button>
                    </div>
                <?php } else { ?>
                    <br />
                    <br />
                <?php } ?>

                <?php if (strlen($partial_deliveries_body) > 0) { ?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Delivered</th>
                                <th>Progress</th>
                                <?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
                                    <th style="width: 100px;">Actions</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php print $partial_deliveries_body; ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    There is no record of deliveries for this order.
                <?php } ?>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save Order</button>
                &nbsp;<button type="reset" class="btn">Cancel</button>
                <a href="<?php print url_for('orders'); ?>" class="btn pull-right">Back</a>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to change the number of products for this order or to cancel it entirely.</p>
            <br />
            <p>You can mark products as delivered gradually by <u>Recording a partial delivery</u>.</p>
            <br />
            <p>If you mark an order as arrived, a Delivery will be <u>automatically</u> added with all remaining products.</p>
            <br />
            <p>If you cancel an order after it has arrived, you need to cancel the Delivery <u>manually</u>.</p>
        </div>
    <?php } ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#add-partial-delivery').click(function(event) {
            $('#add-partial-amount').toggleClass('hidden');
            if ($('#add-partial-amount').hasClass('hidden')) {
                $('#add-partial-amount :input').attr('disabled', 'disabled');
            } else {
                $('#add-partial-amount :input').removeAttr('disabled');
            }
            event.preventDefault();
        });
    });
</script>
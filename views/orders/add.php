<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>orders/add" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" autofocus="autofocus" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="amount">Amount</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                            <input class="input-small" id="amount" name="amount" type="number" pattern="\d+" min="1" step="1" />
                        </div>
                    </div>
                </div>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add Order</button>&nbsp;<button type="reset" class="btn">Cancel</button>
                <a href="<?php print url_for('orders'); ?>" class="btn pull-right">Back</a>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to add products to your existing stock. Make sure that the correct name has been filled in.</p>
            <br />
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        $('#name').typeahead({
            source: <?php print json_encode($names); ?>,
        });
    });
</script>

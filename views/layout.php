<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php print ApplicationName; ?> - <?php print $title; ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php print option('base_uri'); ?>public/img/logo.ico">

    <?php if ($_SERVER['REQUEST_URI'] == option('base_uri')) { ?>
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/jquery-ui.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/ui.daterangepicker.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/dashboard.css" />
    <?php } else if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . 'stock/') === 0)  { ?>
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/jquery-ui.css" />
    <?php } ?>
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/bootstrap.responsive.css" />
    <link rel="stylesheet" href="<?php print option('base_uri'); ?>public/css/layout.css" />

    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/jquery.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/common.js"></script>

    <?php if ($_SERVER['REQUEST_URI'] == option('base_uri')) { ?>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
    </script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/date.js"></script>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/daterangepicker.jQuery.js"></script>
    <?php } else if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . 'stock/') === 0)  { ?>
    <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/jquery-ui.js"></script>
    <?php } ?>
    <!-- <script type="text/javascript" src="<?php print option('base_uri'); ?>public/js/daterangepicker.jQuery.compressed.js"></script> -->
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="<?php print option('base_uri'); ?>"><?php print ApplicationName; ?></a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <?php if ($_SESSION['CurrentUser_ID'] != null) { ?>
                            <?php if ($_SERVER['REQUEST_URI'] == option('base_uri')) { ?>
                            <li class="active"><a href="<?php print option('base_uri'); ?>">Dashboard</a></li>
                            <?php } else { ?>
                            <li><a href="<?php print option('base_uri'); ?>">Dashboard</a></li>
                            <?php } ?>
                            <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "stock") === 0) { ?>
                            <li class="active"><a href="<?php print option('base_uri'); ?>stock">Stock</a></li>
                            <?php } else { ?>
                            <li><a href="<?php print option('base_uri'); ?>stock">Stock</a></li>
                            <?php } ?>
                            <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "orders") === 0) { ?>
                            <li class="active"><a href="<?php print option('base_uri'); ?>orders">Orders</a></li>
                            <?php } else { ?>
                            <li><a href="<?php print option('base_uri'); ?>orders">Orders</a></li>
                            <?php } ?>
                            <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "products") === 0) { ?>
                            <li class="active"><a href="<?php print option('base_uri'); ?>products">Products</a></li>
                            <?php } else { ?>
                            <li><a href="<?php print option('base_uri'); ?>products">Products</a></li>
                            <?php } ?>
							<?php if ($_SESSION["CurrentUser_IsAdministrator"] == "1") { ?>
                                <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "audit") === 0) { ?>
                                <li class="active"><a href="<?php print option('base_uri'); ?>audit">Audit</a></li>
                                <?php } else { ?>
                                <li><a href="<?php print option('base_uri'); ?>audit">Audit</a></li>
								<?php } ?>
                                <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "users") === 0) { ?>
                                <li class="active"><a href="<?php print option('base_uri'); ?>users">Users</a></li>
                                <?php } else { ?>
                                <li><a href="<?php print option('base_uri'); ?>users">Users</a></li>
                                <?php } ?>
							<?php } ?>
                        <?php } ?>
                        <?php if (strpos($_SERVER['REQUEST_URI'], option('base_uri') . "versions") === 0) { ?>
                        <li class="active"><a href="<?php print option('base_uri'); ?>versions">Versions</a></li>
                        <?php } else { ?>
                        <li><a href="<?php print option('base_uri'); ?>versions">Versions</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php print $_SESSION['CurrentUser_Name']; ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php if ($_SESSION['CurrentUser_ID'] != null) { ?>
                                <li><a href="<?php print option('base_uri'); ?>users/<?php print $_SESSION['CurrentUser_ID']; ?>">Edit Profile</a></li>
                                <li><a href="<?php print option('base_uri'); ?>logout">Logout</a></li>
                                <?php } else { ?>
                                <li><a href="<?php print option('base_uri'); ?>login">Login</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content">
            <div class="page-header">
                <h1><?php print $title?></h1>
            </div>
            <?php if ($_GET['error'] != null) { ?>
            <div class="alert alert-error persist">
                <strong>Error:</strong> <?php print $_GET['error']; ?>
            </div>
            <?php } ?>
            <?php if ($_GET['warning'] != null) { ?>
            <div class="alert alert-warning persist">
                <strong>Warning:</strong> <?php print $_GET['warning']; ?>
            </div>
            <?php } ?>
            <?php if ($_GET['success'] != null) { ?>
            <div class="alert alert-success persist">
                <strong>Success:</strong> <?php print $_GET['success']; ?>
            </div>
            <?php } ?>
            <?php if ($_GET['info'] != null) { ?>
            <div class="alert alert-info persist">
                <strong>Information:</strong> <?php print $_GET['info']; ?>
            </div>
            <?php } ?>
            <?php print $content?>
        </div>
        <footer>
            <p><a href="<?php print option('base_uri'); ?>"><?php print ApplicationName; ?></a> is powered by <a href="http://github.com/mbmccormick/limoncello" target="_blank">Limoncello</a>. Version <a href="<?php print url_for('versions'); ?>"><?php print Version; ?></a>.</p>
        </footer>
    </div>
    </body>
</html>

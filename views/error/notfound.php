<div class="content">
    <div class="navigation">
        <?php

            if ($type == "user")
                echo "<a href='/user'>Users</a> / <a href='#'>Not Found</a>";

        ?>
    </div>
    <div class="list">
        <div class="list-item">
            <h3><?php print ucfirst($type); ?> Not Found</h3>
            <br />
            <p>We couldn't find that <?php print $type; ?> for some reason. This could be because that <?php print $type; ?> has been deleted or we may have sent you to a bad link.</p>
            <br />
            <p>If you think you are seeing this page in error, please <a href="mailto:<?php print EmailAddress; ?>">contact</a> your administrator.</p>
        </div>
    </div>
</div>
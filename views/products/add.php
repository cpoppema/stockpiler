<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>products/add" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" autofocus="autofocus" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="code">Code</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-barcode"></i></span>
                            <input class="input-xlarge" id="code" name="code" type="text" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="minimumstock">Minimum Stock</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                            <input class="input-small" id="minimumstock" name="minimumstock" type="number" pattern="\d+" min="1" step="1" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="isdeleted" value="1" /> This product is deleted.
                        </label>
                    </div>
                </div>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add Product</button>&nbsp;<button type="reset" class="btn">Cancel</button>
                <a href="<?php print url_for('products'); ?>" class="btn pull-right">Back</a>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to add a new product to the application. Make sure that the correct code has been filled in, otherwise bad things will happen when you think you do the right thing.</p>
            <br />
            <h5>Minimum Stock</h5>
            <p>The minimum stock is the value to indicate when the system should send out warning e-mails.</p>
            <br />
        </div>
    <?php } ?>
</div>
<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
		<?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
			<div class="well">
				<a href="<?php print option('base_uri'); ?>products/add" class="btn btn-primary">New Product</a>
			</div>
		<?php } ?>
        <div class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-font"></i></span>
                    <input class="input-xlarge" id="name" name="name" type="text" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="code">Code</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-barcode"></i></span>
                    <input class="input-xlarge" id="code" name="code" type="text" />
                </div>
            </div>
        </div>
        <br />
        Filter products:
        <div class="btn-toolbar">
            <div class="btn-group" data-toggle="buttons-radio" id="product-filter">
                <button type="button" class="btn<?php if(!$_SESSION['CurrentUser_HideInacitve']) { ?> active<?php } ?>" data-filter="all">All</button>
                <button type="button" class="btn<?php if($_SESSION['CurrentUser_HideInacitve']) { ?> active<?php } ?>" data-filter="hide" data-toggle="button">No Deleted</button>
                <button type="button" class="btn" data-filter="show">Deleted</button>
                <?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
                    <button type="button" class="btn" data-filter="stock">Need moar</a>
                <?php } ?>
            </div>
        </div>
        <br />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th style="width: 60px;">Expected stock</th>
                    <th style="width: 60px;">Technical stock</th>
                    <th style="width: 60px;">Minimum stock</th>
                    <?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
						<th style="width: 100px;">Actions</th>
					<?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php print $body; ?>
            </tbody>
        </table>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page shows the list of products you're able to scan in/out for the application. This list allows you to view a product's stock
            <?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?> or create a new product<?php } ?>. The stock shows <abbr title="Number of available products in stock" rel="tooltip">expected</abbr>, <abbr title="Number of products in your stockroom" rel="tooltip">technical</abbr> and <abbr title="Warning e-mails will be send if the expected stock goes below this number" rel="tooltip">minimum</abbr> stock.
            <br />
            <br />Type in a name to filter the products listed.</p>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        function filter_on_name(name) {
            if (name != '') {
                $(rows).find('th.name:icontains(' + name + ')').closest('tr').removeClass('hide');
                $(rows).find('th.name:not(:icontains(' + name + '))').closest('tr').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_code(code) {
            if (code != '') {
                $(rows).find('td.code:contains(' + code + ')').closest('tr').removeClass('hide');
                $(rows).find('td.code:not(:contains(' + code + '))').closest('tr').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_isdeleted(filter) {
            if (filter != 'all') {
                if (filter == 'show') {
                    $('table tbody tr.deleted').removeClass('hide');
                    $('table tbody tr:not(tr.deleted)').addClass('hide');
                } else if (filter == 'stock') {
                    $('table tbody tr.error:not(tr.deleted)').removeClass('hide');
                    $('table tbody tr:not(tr.error), table tbody tr.error.deleted').addClass('hide');
                } else {
                    $('table tbody tr:not(tr.deleted)').removeClass('hide');
                    $('table tbody tr.deleted').addClass('hide');
                }
            } else {
                $(rows).removeClass('hide');
            }
        }

        var rows = $('table tbody tr');

        // check if a code is filled in when javascript has loaded
        var code = $('#code').val();
        if (code != '') {
            filter_on_code(code);
        } else {
            // check if a name is filled in when javascript has loaded
            var name = $('#name').val();
            if (name != '')
                filter_on_name(name);
        }

        $('#code').bind('input change paste keyup mouseup', function() {
            filter_on_code($(this).val());
        });

        $('#name').bind('input change paste keyup mouseup', function() {
            filter_on_name($(this).val());
        });

        $('#product-filter .btn').click(function(event) {
            $(event.target).button('toggle');
            filter_on_isdeleted($(this).data('filter'));
        });

        $('#name').typeahead({
            source: <?php print json_encode($names); ?>,
        });

        $('#code').typeahead({
            source: <?php print json_encode($codes); ?>,
        });
    });
</script>

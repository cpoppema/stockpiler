<div class="row">
    <div class="span6">
        <div class="control-group">
            <label class="control-label" for="name">Name:</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-font"></i></span>
                    <input class="input-x-large" id="name" name="name" type="text" autocomplete="off" />
                </div>
            </div>
        </div>
        <br />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th style="width: 60px;">Stock</th>
                </tr>
            </thead>
            <tbody>
                <?php print $body; ?>
            </tbody>
        </table>
    </div>
    <div class="span2">
		<br /><br /><br /><br /><br /><br /><br />
        <h5 class="alert alert-block alert-success persist">In stock</h5>
        <h5 class="alert alert-block alert-warning persist">Below Minimum Stock</h5>
        <h5 class="alert alert-block alert-error persist">Out Of Stock.</h5>
    </div>
</div>

<script>
    $(document).ready(function() {
        function filter_on_name(name) {
            if (name != '') {
                $(rows).find('th.name:icontains(' + name + ')').closest('tr').removeClass('hide');
                $(rows).find('th.name:not(:icontains(' + name + '))').closest('tr').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        var rows = $('table tbody tr');

		// check if a name is filled in when javascript has loaded
		var name = $('#name').val();
		if (name != '')
			filter_on_name(name);

        $('#name').bind('input change paste keyup mouseup', function() {
            filter_on_name($(this).val());
        });

        $('#name').typeahead({
            source: <?php print json_encode($names); ?>,
			updater:function (item) {
                //item = selected item

                //do your stuff.
                filter_on_name($('#name').val());

                //dont forget to return the item to reflect them into input
                return item;
            }
        });
    });
</script>

<div class="span6">
	<div id="order_chart"></div>
	&nbsp;
</div>
<div class="span2">
	<table class="table table-condensed table-striped table-hover">
		<thead>
			<tr>
				<th colspan="2" class="alert alert-block alert-success persist">Orders</th>
			</tr>
			<tr>
				<th class="alert alert-block alert-success persist">Name</th>
				<th class="alert alert-block alert-success persist">Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php print $orders_table; ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
  	//google.setOnLoadCallback(drawChart, true);
	$(document).ready(function() {
	  	function drawChart() {
		  	// Draw orders
		    var data = google.visualization.arrayToDataTable([
				['Task', 'Orders per product'],
				<?php print $orders_data; ?>
		    ]);

		  	// Create and draw the visualization.
		    new google.visualization.PieChart(document.getElementById('order_chart'))
		    	.draw(data, {title: 'Orders per product', sliceVisibilityThreshold: 0, is3D: true, pieSliceText: 'value'});
		}
		drawChart();
	});
</script>
<!--
<style type="text/css">
	.stock-change a {
		background: transparent;
		position: absolute;
		width: 100%;
		text-align: center;
	}

	.stock-change {
		position: relative;
	}

	.stock-change a,
	.stock-change .error {
		/*float: left;*/
		height: 100%;
		/*color: white !important;*/
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		margin: 0px;
		display: inline-block;
	}

	.just.error.stock-change a,
	.error.stock-change .error {
		width:100%;
		background-color: red;
	}

	.success.stock-change {
		width:100%;
		background-color: green;
	}

	.success.half {
		width:50%;
	}

	.error.stock-change.half .error {
		width:50%;
		float: right;
	}

	.ui-datepicker-week-end {
		display: none;
	}
	.ui-datepicker-week-col {
		text-align: center;
		background: whiteSmoke;
	}
	.ui-datepicker td.active a {
		background:#0064cd;
		color:white;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
	}
	.daterangepicker .ui-widget-header {
		border: none;
	}
</style>

<table class="ui-datepicker" style="display: block">
<td class="stock-change success error half" style="width: 100px; height: 20px;">
	<div class="error"></div>
	<a class="ui-state-default" href="#">16</a>
</td>
</table>

<script>
// $('.stock-change.success.error.half').prepend($('<div>').addClass('error'));
</script>

<div style="width: 100px; height: 20px; position: relative;">
	<div class="success calendar-cell-background half"></div>
	<div class="error calendar-cell-background half"></div>
	<div class="calendar-cell-value">20</div>
</div>

<h5>Pickups and deliveries between <?php print params('start') . ' and ' . params('end'); ?></h5>



<?php

$deliveries = array();
$pickups = array();
$result = mysql_query('SELECT name, SUM(amount) AS amount, type, product_id '.
					  'FROM products_stock '.
					  '  JOIN products_product ON products_stock.product_id = products_product.id '.
					  'WHERE iscanceled=0 '.
					  'GROUP BY type, product_id ORDER BY name ASC');
while( $row = mysql_fetch_array($result) ) {
	if($row['type'] == 'delivery') {
		$deliveries[] = $row;
	} else {
		$pickups[] = $row;
	}
}

$dates = '';
$result = mysql_query('SELECT name, DATE_FORMAT(date, \'%d-%c-%Y\') AS date, amount, `type` '.
					  'FROM products_stock '.
					  '  JOIN products_product ON products_stock.product_id = products_product.id '.
					  '  JOIN log ON log.foreign_id = products_stock.id '.
					  'WHERE action=\'created\' AND foreign_type = \'stock\' AND  iscanceled=0 '.
					  'ORDER BY name ASC');
while( $row = mysql_fetch_array($result) ) {
	$dates[$row['date']][$row['type']][] = array('name' => $row['name'], 'amount' => $row['amount']);
}

?>

<?php

// if day
// if more > day
  	// if week
	// if multi day
// if month
// if multi month
// if multi year

?>

<div id="inline-datepicker"></div>

<script>
	var dates = <?php print json_encode(array(params('start'), params('end'))); ?>;
	var dateStart = Date.parse(dates[0]);
	var dateEnd = Date.parse(dates[1]);
	var maxDate = Date.today();

	var extendedInformation = <?php print json_encode($dates); ?>;

	// function to format a date string
	function fDate(date){
	   if(date == null || !date.getDate()){return '';}
	   var day = date.getDate();
	   var month = date.getMonth();
	   var year = date.getFullYear();
	   month++; // adjust javascript month
	   var dateFormat = 'dd-mm-yy';
	   return $.datepicker.formatDate( dateFormat, date );
	}

	$('#inline-datepicker').datepicker({
		numberOfMonths: [1, 2],
		beforeShowDay: function(date_original) {
			var date = new Date(date_original); // don't mutate jquery's date object used for iteratation
			var background_classes = '';
			if (extendedInformation[fDate(date)] != null) {
				if (extendedInformation[fDate(date)]['delivery'] != null && extendedInformation[fDate(date)]['pickup'] != null) {
					background_classes = 'stock-change alert-success alert-error half';
				} else {
					if (extendedInformation[fDate(date)]['delivery'] != null) {
						background_classes = 'stock-change alert-success';
					} else if (extendedInformation[fDate(date)]['pickup'] != null) {
						background_classes = 'stock-change alert-error';
					}
				}
			}


			if ( !(date.is().saturday() || date.is().sunday()) && (date.equals(dateStart) || date.equals(dateEnd)) ) {
				if ( date.equals(dateEnd) ) {
					return [true, 'active ui-datepicker-current-day ' + background_classes];
				}
				return [true, 'active ' + background_classes];
			}

			if (date.is().monday()) {
				if ( (dateStart.is().sunday()) && (date.last().sunday().equals(dateStart)) ||
					 (dateStart.is().saturday() && date.last().saturday().equals(dateStart)) ) {
					return [true, 'active ' + background_classes];
				}
			} else if (date.is().friday() && ( dateEnd.is().saturday() || dateEnd.is().sunday()) ) {
				if ( maxDate.compareTo(dateEnd) == -1) {
					if (date.next().saturday().equals(maxDate) || date.next().sunday().equals(maxDate)) {
						return [true, 'active ' + background_classes];
					}
				} else {
					if (date.next().saturday().equals(dateEnd) || date.next().sunday().equals(dateEnd)) {
						return [true, 'active ' + background_classes];
					}
				}
			}

			return [!(date_original.is().saturday() || date_original.is().sunday()), background_classes];
		},
		showWeek: true,
		maxDate: '0',
		gotoCurrent: true,
		// regional: $.datepicker.regional[ "en" ]
	});
	$('#inline-datepicker').datepicker( "setDate", new Date(dateEnd));
</script>

<br />
<div class="row">
	<div class="span2">
		<table class="table table-condensed table-striped table-hover">
			<thead>
				<tr class="succes">
					<th colspan="2" class="alert alert-block alert-success persist">Deliveries</th>
				</tr>
				<tr class="succes">
					<th class="alert alert-block alert-success persist">Name</th>
					<th class="alert alert-block alert-success persist">Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($deliveries as $row) { ?>
				<tr>
					<td><?php print $row['name']; ?></td>
					<td>&plus; <?php print abs($row['amount']); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="span2 offset2">
		<table class="table table-condensed table-striped table-hover">
			<thead>
				<tr class="error">
					<th colspan="2" class="alert alert-block alert-error persist">Pickups</th>
				</tr>
				<tr class="error">
					<th class="alert alert-block alert-error persist">Name</th>
					<th class="alert alert-block alert-error persist">Amount</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($pickups as $row) { ?>
				<tr>
					<td><?php print $row['name']; ?></td>
					<td>&minus; <?php print abs($row['amount']); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
-->

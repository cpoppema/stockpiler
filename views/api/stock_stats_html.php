<div class="span6">
	<div id="pickupdelivery_chart" class="chart"></div>
	<div id="delivery_chart" class="chart"></div>
	<div id="pickup_chart" class="chart"></div>
	&nbsp;
</div>
<div class="span2">
	<table class="table table-condensed table-striped table-hover">
		<thead>
			<tr>
				<th colspan="2" class="alert alert-block alert-success persist">Deliveries</th>
			</tr>
			<tr>
				<th class="alert alert-block alert-success persist">Name</th>
				<th class="alert alert-block alert-success persist">Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php print $deliveries_table; ?>
		</tbody>
	</table>
	<table class="table table-condensed table-striped table-hover">
		<thead>
			<tr>
				<th colspan="2" class="alert alert-block alert-error persist">Pickups</th>
			</tr>
			<tr>
				<th class="alert alert-block alert-error persist">Name</th>
				<th class="alert alert-block alert-error persist">Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php print $pickups_table; ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
  	//google.setOnLoadCallback(drawChart, true);
	$(document).ready(function() {
	  	function drawChart() {
		  	// Draw delivery vs. pickup
		    var data = google.visualization.arrayToDataTable([
				['Task', 'Deliveries and pickup'],
				['Deliveries', <?php print $deliveries_count; ?>],
				['Pickups',    <?php print $pickups_count; ?>],
		    ]);

		  	// Create and draw the visualization.
		    new google.visualization.PieChart(document.getElementById('pickupdelivery_chart'))
		    	.draw(data, {title: 'Deliveries and pickups', colors: ['#5BB75B','#DA4F49'], sliceVisibilityThreshold: 0, is3D: true, pieSliceText: 'value'});

		  	// Draw deliveries
		    var data = google.visualization.arrayToDataTable([
				['Task', 'Deliveries per product'],
				<?php print $delivery_data; ?>
		    ]);

		  	// Create and draw the visualization.
		    new google.visualization.PieChart(document.getElementById('delivery_chart'))
		    	.draw(data, {title: 'Deliveries per product', sliceVisibilityThreshold: 0, is3D: true, pieSliceText: 'value'});

		  	// Draw pickups
		    var data = google.visualization.arrayToDataTable([
				['Task', 'Pickups per product'],
				<?php print $pickup_data; ?>
		    ]);

		  	// Create and draw the visualization.
		    new google.visualization.PieChart(document.getElementById('pickup_chart'))
		    	.draw(data, {title: 'Pickups per product', sliceVisibilityThreshold: 0, is3D: true, pieSliceText: 'value'});
		}
		drawChart();
	});
</script>
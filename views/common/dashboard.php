<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
		<div class="well">
            <div id="calendar-type" class="btn-group" data-toggle="buttons-radio">
                <button type="button" class="btn active" data-filter="stock" data-toggle="button">Stock</button>
                <button type="button" class="btn" data-filter="order" data-toggle="button">Orders</button>
        	</div>
            <br />
           <!--  <div id="calendar-range" class="btn-group" data-toggle="buttons-radio">
				<button class="btn active" data-start="<?php $d = new DateTime( ); $d->modify( 'last monday' ); echo $d->format('d-m-Y'); ?>" data-end="<?php $d->modify( 'next friday' ); echo $d->format('d-m-Y'); ?>">Last Week</button>
				<button class="btn" data-start="<?php $d = new DateTime( ); $d->modify( 'first day of last month' ); echo $d->format('d-m-Y'); ?>" data-end="<?php $d->modify( 'last day of month' ); echo $d->format( 'd-m-Y' ); ?>">Last Month</button>
				<button class="btn" data-end="<?php $d = new DateTime( ); echo $d->format('d-m-Y'); ?>" data-start="<?php  $d->modify( 'first day of month' ); echo $d->format('d-m-Y'); ?>">This Month</button>
				<button id="select-dates" class="btn btn-primary">Custom</button>
            </div> -->

	        <div id="daterangepicker" class="control-group daterangepicker">
	            <div class="controls controls-row">
	                <div class="input-prepend btn-group">
	                    <button id="datepicker-btn" class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
	                    <input class="input-small" id="start" name="start" type="text" placeholder="Start Date" value="<?php $d = new DateTime( ); $d->modify( 'last friday' ); $d->modify( 'last monday'); echo $d->format('d-M-Y'); ?>" />
                    	<span>to</span>
                    	<input class="input-small" id="end" name="end" type="text" placeholder="End Date" value="<?php $d = new DateTime( ); $d->modify( 'last friday' ); echo $d->format('d-M-Y'); ?>" />
	                </div>
	            </div>
	        </div>
		</div>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
	    <div class="span2">
	        <h5>Page Description</h5>
	        <p>This page allows you to see exactly the amount of products coming in and out of your stock over a set amount of time.</p>
	    </div>
	<?php } ?>
</div>
<div class="row">
	<div id="stats"></div>
</div>
<script>
    $(document).ready(function() {
    	// function load_calendar(start, end) {
    	function load_calendar() {
			var start = $('#start').val(), end = $('#end').val();
    		if (start && end) {
    			if (Date.parse(end).compareTo(Date.parse(start)) < 0) {
    				$('<span>')
    					.addClass('help-inline')
    					.text('Did you really think this would work? Please..')
    					.appendTo($('#end').closest('.controls'));
    			} else {
					$('#daterangepicker').find('.help-inline').remove();
		    		$('#stats').load(
		    			'<?php print option('base_uri'); ?>api/html/stats/' + $('#calendar-type .btn.active').data('filter') + '/' + encodeURIComponent(start.replace(/\//g, '-')) + '/' + encodeURIComponent(end.replace(/\//g, '-'))
		    		);
		    	}
	    	}
    	}

		$('#calendar-type .btn').click(function(event) {
			$(event.target).button('toggle');
			load_calendar();
			// if (e.target != $('#select-dates')[0]) {
   //  			// var start = $('#calendar-range .active').data('start'), end = $('#calendar-range .active').data('end');

   //  			var start = $('#start').val(), end = $('#end').val();
   //  			load_calendar(start, end);
   //  		}
		});

   //  	$('#calendar-range .btn').click(function(e) {
			// $(e.target).button('toggle');
   //  		if (e.target == $('#select-dates')[0]) {
	  //   		$('#daterangepicker').slideDown(function() {
	  //   			if (!$('#daterangepicker').hasClass('haspicker')) {
	  //   				$('#daterangepicker').addClass('haspicker');
	  //   				$('#start, #end').daterangepicker();
	  //   			}
	  //   		});
	  //   	} else {
	  //   		$('#daterangepicker').fadeOut('fast');
			// 	var start = $('#calendar-range .active').data('start'), end = $('#calendar-range .active').data('end');
			// 	load_calendar(start, end);
	  //   	}
   //  	});


		// function to format a date string
		function fDate(date){
		   if(date == null || !date.getDate()){return '';}
		   var day = date.getDate();
		   var month = date.getMonth();
		   var year = date.getFullYear();
		   month++; // adjust javascript month
		   var dateFormat = 'dd-mm-yy';
		   return $.datepicker.formatDate( dateFormat, date );
		}

		// try to mark an option as active if the ranges match
		$('#datepicker-btn').click(function() {
			var daterangepicker = $('#daterangepicker .dropdown-menu:first');
			$.each(daterangepicker.find('li'), function(){
				var active_class = 'active';
				daterangepicker.find('.active').removeClass('active');

				if ( $(this).data('dateStart') && $(this).data('dateEnd')) {
					var dateStart = (typeof $(this).data('dateStart') == 'string') ? Date.parse($(this).data('dateStart')) : $(this).data('dateStart')();
					var dateEnd = (typeof $(this).data('dateEnd') == 'string') ? Date.parse($(this).data('dateEnd')) : $(this).data('dateEnd')();

					if($('#start').val().replace(/\//g, '-') == fDate(dateStart) && $('#end').val().replace(/\//g, '-') == fDate(dateEnd)) {
						$(this).addClass('active');
						return false;
					}
				}
			});
		});

		// enable daterangepicker on both start and end input
		$('#start, #end').daterangepicker({
		 	dateFormat: 'dd-M-yy',
		});

		// load calendar ...
		// .. when hitting enter
		$('#start, #end').keyup(function(event) {
			var rp = $('.ui-daterangepicker:visible');
			rp.fadeOut(300).data('state', 'closed');
			if (event.keyCode == 13) {
    			load_calendar();
			}
		});

		// .. when clicking a non-submenu option
		$('.dropdown-menu > li:not(.dropdown-submenu) > a').click(function(e) {
			$(e.target).closest('.btn-group').find('.dropdown-toggle').dropdown('toggle');
			load_calendar();
		});

		// .. on page load
		load_calendar();

		// don't close when clicking in submenu's
		$('.dropdown-menu').click(function(event) {
			event.stopPropagation();
		});
    });
</script>


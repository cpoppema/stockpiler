<?php

	require_once Root . '/library/swiftmailer/swift_required.php';

	$subject_clean = 'Stockpiler: please buy new {product} >> {tech_stock}/{min_stock}';
	$body_clean = file_get_contents(realpath(dirname(__file__)) . '/email.template.php');
	$recipients = array();

	$transport = Swift_MailTransport::newInstance();
	$mailer = Swift_Mailer::newInstance($transport);

?>

<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        &nbsp;

<?php

	$recipients = R::find('user', 'receivewarningemails = 1');

	if (R::$adapter->getAffectedRows() > 0)
	{

        $products = R::$f->begin()
            ->select('product.*, ')
                ->open()
                    ->select('SUM(amount)')->from('stock')->where('stock.product_id = product.id')->and('iscanceled = 0 or iscanceled is null')
                ->close()->as('amount')
            ->from('product')
            ->where('isdeleted = 0 or isdeleted is null')
            ->having('amount < minimumstock')
            ->order_by('name asc, code asc')
            ->get();

		foreach($recipients as $recipient)
		{
			?>

			<dl>
		  		<dt>Sent an e-mail to <?php print $recipient['name']; ?> &lt;<?php print $recipient['email']; ?>&gt; for:</dt>

			<?php

			foreach($products as $product)
			{
				$subject = str_replace(
					array("{product}", "{tech_stock}", "{min_stock}"),
					array($product['name'], $product['amount'], $product['minimumstock']),
					$subject_clean
				);
				$body = str_replace(
					array("{name}", "{product}", "{expected_stock}", "{technical_stock}", "{minimum_stock}", "{version}", "{site}", "{date}"),
					array($recipient['name'], $product['name'], $product['amount'] + $product['expectedstock'], $product['amount'], $product['minimumstock'], Version, Site, date('Y-m-d H:i:s')),
					$body_clean
				);

				$message = Swift_Message::newInstance($transport)
				->setSubject($subject)
				->setFrom(EmailAddress, ApplicationName)
				->setTo($recipient['email'], $recipient['name'])
				->setBody($body);

				$mailer->send($message);

				?>

				<dd><?php print $product['name']; ?> >> <?php print $product['amount']; ?> / <?php print $product['minimumstock']; ?></dd>

				<?php
			}

			?>

			</dl>

			<?php
		}
	}

?>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
	    <div class="span2">
	        <h5>Page Description</h5>
	        <p>This page sends e-mails for products that are below its minimum stock to users who specifically said they wanted to receive them.</p>
	        <br />
	    </div>
    <?php } ?>
</div>
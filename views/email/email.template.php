Howdy {name},

Red Alert 2! We (actually it's just you) are running out of {product}. Go get some..

Stock information
expected: {expected_stock}
technical: {technical_stock}
minimum: {minimum_stock}

Sincerely,
Stockpiler


{site}
version: {version}

This e-mail was automatically generated at {date}


<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <div class="control-group">
            <label class="control-label" for="text">Full text search</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-font"></i></span>
                    <input class="input-xlarge" id="text" name="text" type="text" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="code">Code</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-barcode"></i></span>
                    <input class="input-xlarge" id="code" name="code" type="text" />
                </div>
            </div>
        </div>
        <br />
        Show audit trail for:
        <div class="btn-toolbar">
            <div class="btn-group" data-toggle="buttons-radio" id="user-filter">
                <button type="button" class="btn active" data-filter="all">Everyone</button>
                <button type="button" class="btn" data-filter="not-me" data-toggle="button">All but me</button>
                <button type="button" class="btn" data-filter="me">Me</button>
            </div>
        </div>
        <br />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>At what time</th>
                    <th>happened what?</th>
                </tr>
            </thead>
            <tbody>
                <?php print $body; ?>
            </tbody>
        </table>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page shows the audit log for every change to stock or product.

            <br />
            <br />Type in a code or name to filter the audit log entries listed.</p>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        function filter_on_text(text) {
            if (text != '') {
                $(rows).find('td.text:icontains(' + text + ')').closest('tr').removeClass('hide');
                $(rows).find('td.text:not(:icontains(' + text + '))').closest('tr').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_code(code) {
            if (code != '') {
                $(rows).find('td.code').not('[data-code*="' + code + '"]').closest('tr').addClass('hide');
                $(rows).find('td.code[data-code*="' + code + '"]').closest('tr').removeClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_user(filter) {
            if (filter != 'all') {
                $('table tbody tr[data-filter="' + filter + '"]').removeClass('hide');
                $('table tbody tr').not('[data-filter="' + filter + '"]').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function toggle_text(button) {
            var text = $(button).text();
            $(button).text($(button).data('toggle-text'));
            $(button).data('toggle-text', text);
        }

        rows = $('table tbody tr');

        // check if a code is filled in when javascript has loaded
        var code = $('#code').val();
        if (code != '') {
            filter_on_code(code);
        } else {
            // check if a name is filled in when javascript has loaded
            var name = $('#text').val();
            if (name != '')
                filter_on_name(name);
        }

        $('#code').bind('input change paste keyup mouseup', function() {
            filter_on_code($(this).val());
        });

        $('#text').bind('input change paste keyup mouseup', function() {
            filter_on_text($(this).val());
        });

        $('#user-filter .btn').click(function(event) {
            $(event.target).button('toggle');
            filter_on_user($(this).data('filter'));
        });

        $('#text').typeahead({
            source: <?php print json_encode($names); ?>,
        });

        $('#code').typeahead({
            source: <?php print json_encode($codes); ?>,
        });

        // check if buttongorup is active when javascript has loaded
        if ($('#user-filter').hasClass('active')) {
            toggle_text($('#user-filter'));
        }
    });
</script>

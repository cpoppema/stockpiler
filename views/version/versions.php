<!-- <h3>2013-02-28</h3> GUESS I WAS RIGHT! -->

<!-- <ul> -->
	<!-- <li>You need to tell me why you cancel stock changes and orders. Why? Because!</li> -->
<!-- </ul> -->

<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>


		<h3>2013-04-17</h3>

		Per request:
		<ul>
			<li>Delivery (bulk) and pickup (bulk) now have a manual mode, uses product names instead of codes. Enable by ticking in your profile options: *Manual mode instead of hand scanner mode in delivery and pickup by default*</li>
		</ul>


		<h3>2013-02-28</h3>

		Some new awesome admin POWERRR:
		<ul>
			<li>Audit log. We'll be wachting your every move :) Awww wait, already happened! BUT! Now every admin can look into it as well....</li>
			<li>Only the chosen ones are now able to easily undo deleting an account yay!</li>
		</ul>


		<h3>2013-01-10</h3>

		Happy New Year AND:
		<ul>
			<li>Updated hashes system a bit! It is now configurable in the.. configurati0n!</li>
		</ul>

		<h3>2012-11-09</h3>

		Look:
		<ul>
			<li>You can now record partially deliveries on the edit order screen! (:</li>
		</ul>

		<h3>2012-11-01</h3>

		Invisible changes
		<ul>
			<li>Now using an ORM: RedBeanPHP. Read up on it at <a href="http://www.redbeanphp.com/manual2_0/">http://www.redbeanphp.com/manual2_0/</a> or just look at examples in the source or across the net for JIT knowledge! :)</li>
		</ul>

		<h3>2012-10-31</h3>

		Some additions while improving the code generally
		<ul>
			<li>Orders can be marked as arrived and will create a Delivery automatically! (This delivery won't be canceled when you cancel the order afterwards... FYI: I'm NOT lazy, I think)</li>
		</ul>

		<h3>2012-10-22</h3>

		Admin no longer lives
		<ul>
			<li>Made accounts not really deletable</li>
		</ul>

		<h3>2012-10-21</h3>

		Dates everywhere! Oh wait nope, didn't work as easy, so only presets and manual input on the dashboard for now: no fancy datepickers! Maybe some day..
		<ul>
			<li>Made page descriptions optional</li>
			<li>Added a little pickup & delivery statistics to dashboard</li>
		</ul>

		<h3>2012-10-19</h3>
		Here you go
		<ul>
			<li>Added read-only option for users</li>
			<li>Created link to display list of product with technical stock to use as an iframe widget in SugarCRM</li>
		</ul>

		<h3>2012-10-18</h3>

		A long day
		<ul>
			<li>yea it works.. : delivery and pickup BULK <i class="icon-ok"></i> kudos!</li>
			<li>Added option to choose bulk as default</li>
			<li>Delivery and pickup both verify code via AJAX, ubercool but lacking a busy indicator</li>
		</ul>

		<h3>2012-10-17</h3>

		All's kewl.
		<ul>
			<li>Added option to hide deleted products, canceled stock, canceled orders by default (doesn't combine very well with the search)</li>
			<li>NEW: orders, which also happen to influence the expected stock</li>
			<li>Added expected stock</li>
			<li>Added typeahead for product names in overviews</li>
			<li>Added typeahead for product codes in overviews</li>
			<li>Send spam to users who agreed to get 'sum</li>
		</ul>

		<h3>2012-10-16</h3>

		Another glorious day.
		<ul>
			<li>Products list</li>
			<li>Product add</li>
			<li>Product edit</li>
			<li>Added a brief explanation for expected, technical and minimum stock to the products list.</li>
			<li>Added an opt-in on user profile to receive stock warning e-mails.</li>
			<li>Adding or editing a product puts your fingerprint on it! Don't be stupid, we'll find out..</li>
			<li>Enabled Tooltip plug-in</li>
			<li>Case-insensitive javascript search in products list and stock history</li>
			<li>Made stock sorta work flawlessy!!</li>
		</ul>

		<h3>2012-10-15</h3>

		First concept for Stockpiler in development. It's a historic day. Eureka!
		<ul>
			<li>User management (alright.. this was built-in)</li>
			<li>Concept database layout</li>
			<li>This page!</li>
			<li>Small changes to display some content only if you're logged in</li>
		</ul>

	</div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
		<div class="span2">
			<h5>Page description</h5>
			<p>This page shows the different additions and changes to this applictaion</p>
		</div>
    <?php } ?>
</div>
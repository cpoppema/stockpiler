<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
		<?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
			<div class="well">
				<a href="<?php print option('base_uri'); ?>stock/delivery<?php if($_SESSION['CurrentUser_BulkIsDefault']) { ?>/bulk<?php } ?>" class="btn btn-success">Delivery</a>
				<a href="<?php print option('base_uri'); ?>stock/pickup<?php if($_SESSION['CurrentUser_BulkIsDefault']) { ?>/bulk<?php } ?>" class="btn btn-danger">Pickup</a>
				<a href="#" class="btn btn-primary<?php if($_SESSION['CurrentUser_BulkIsDefault']) { ?> active<?php } ?>" id="bulk-btn" data-toggle="button" data-toggle-text="Bulk">No Bulk</a>
			</div>
		<?php } ?>
        <div class="control-group">
            <label class="control-label" for="name">Name</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-font"></i></span>
                    <input class="input-xlarge" id="name" name="name" type="text" />
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="code">Code</label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-barcode"></i></span>
                    <input class="input-xlarge" id="code" name="code" type="text" />
                </div>
            </div>
        </div>
        <br />
        Filter stock changes:
        <div class="btn-toolbar">
            <div class="btn-group" data-toggle="buttons-radio" id="stock-filter">
                <button type="button" class="btn<?php if(!$_SESSION['CurrentUser_HideInacitve']) { ?> active<?php } ?>" data-filter="all">All</button>
                <button type="button" class="btn<?php if($_SESSION['CurrentUser_HideInacitve']) { ?> active<?php } ?>" data-filter="hide" data-toggle="button">No Canceled</button>
                <button type="button" class="btn" data-filter="show">Canceled</button>
            </div>
        </div>
        <br />
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product</th>
                    <th>Code</th>
                    <th>Change</th>
					<th style="width: 100px;">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php print $body; ?>
            </tbody>
        </table>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page shows the last changes to any product's stock.

            <?php if ($_SESSION["CurrentUser_IsReadOnly"] != "1") { ?>
                <br />
                <br />Click delivery/pickup to change the current stock of products.
            <?php } ?>
            <br />
            <br />Type in a code or name to filter the stock changes listed.</p>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        function filter_on_name(name) {
            if (name != '') {
                $(rows).find('td.name:icontains(' + name + ')').closest('tr').removeClass('hide');
                $(rows).find('td.name:not(:icontains(' + name + '))').closest('tr').addClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_code(code) {
            if (code != '') {
                $(rows).find('td.code').not(':icontains(' + code + ')').closest('tr').addClass('hide');
                $(rows).find('td.code:icontains(' + code + ')').closest('tr').removeClass('hide');
            } else {
                $(rows).removeClass('hide');
            }
        }

        function filter_on_iscanceled(filter) {
            if (filter != 'all') {
                if (filter == 'show') {
                    $('table tbody tr.warning').removeClass('hide');
                    $('table tbody tr:not(tr.warning)').addClass('hide');
                } else {
                    $('table tbody tr:not(tr.warning)').removeClass('hide');
                    $('table tbody tr.warning').addClass('hide');
                }
            } else {
                $(rows).removeClass('hide');
            }
        }

        function toggle_text(button) {
            var text = $(button).text();
            $(button).text($(button).data('toggle-text'));
            $(button).data('toggle-text', text);
        }

        rows = $('table tbody tr');

        // check if a code is filled in when javascript has loaded
        var code = $('#code').val();
        if (code != '') {
            filter_on_code(code);
        } else {
            // check if a name is filled in when javascript has loaded
            var name = $('#name').val();
            if (name != '')
                filter_on_name(name);
        }

        $('#code').bind('input change paste keyup mouseup', function() {
            filter_on_code($(this).val());
        });

        $('#name').bind('input change paste keyup mouseup', function() {
            filter_on_name($(this).val());
        });

        $('#calendar-type .btn').click(function(event) {
            $(event.target).button('toggle');
        });

        $('#stock-filter .btn').click(function(event) {
            $(event.target).button('toggle');
            filter_on_iscanceled($(this).data('filter'));
        });

        $('#name').typeahead({
            source: <?php print json_encode($names); ?>,
        });

        $('#code').typeahead({
            source: <?php print json_encode($codes); ?>,
        });

        // check if bulk button is active when javascript has loaded
        if ($('#bulk-btn').hasClass('active')) {
            toggle_text($('#bulk-btn'));
        }

        $('#bulk-btn').click(function() {
            toggle_text($(this));
            var button = $(this);

            var anchors = $(this).closest('div').find('a');
            $.each(anchors, function(index, anchor) {
                if (!$(button).hasClass('active')) {
                    $(anchor).attr('href', $(anchor).attr('href') + '/bulk');
                } else {
                    $(anchor).attr('href', $(anchor).attr('href').replace(/(\/bulk)/, ''))
                }
            });
        });
    });
</script>

<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0) { ?>
        <div id="handscannermode">
            <form action="<?php print option('base_uri'); ?>stock/pickup" method="post" class="form-vertical">
                <input type="hidden" name="manualmode" value="0" />
                <fieldset class="product">
                    <div class="control-group">
                        <label class="control-label" for="code">Code</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-barcode"></i></span>
                                <input class="input-xlarge" id="code" name="code" type="text" autofocus="autofocus" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="name">Name</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-font"></i></span>
                                <input class="input-xlarge name" id="name" name="name" type="text" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="control-group">
                        <label class="control-label" for="amount">Amount</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                                <input class="input-small amount" id="amount" name="amount" type="number" pattern="\d+" min="1" step="1" />
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br />
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Pickup</button>&nbsp;<button type="reset" class="btn">Cancel</button>
                </div>
            </form>
        </div>
        <?php } else { ?>
        <div id="manualmode">
            <form action="<?php print option('base_uri'); ?>stock/pickup" method="post" class="form-vertical">
                <input type="hidden" name="manualmode" value="1" />
                <fieldset class="product">
                    <input type="hidden" name="id" class="id" required <?php if (!empty($_POST['id'])) print 'value="' . $_POST['id'] . '"'; ?> />
                    <div class="control-group">
                        <label class="control-label" for="name">Name</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-font"></i></span>
                                <input class="input-xlarge name" id="name" name="name" type="text" <?php if (!empty($_POST['name'])) print 'value="' . $_POST['name'] . '"'; ?> />
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="amount">Amount</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                                <input class="input-small amount" id="amount" name="amount" type="number" pattern="\d+" min="1" step="1" <?php if (!empty($_POST['amount'])) print 'value="' . $_POST['amount'] . '"'; ?> />
                            </div>
                        </div>
                    </div>
                </fieldset>
                <br />
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Pickup</button>&nbsp;<button type="reset" class="btn">Cancel</button>
                </div>
            </form>
        </div>
        <?php } ?>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to remove products from your current stock. Make sure that the correct code has been filled in.</p>
            <br />
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        function find_product_by_code(source, code) {
            var jqXHR = $.ajax({
                url: '<?php print option('base_uri'); ?>api/json/product/find/code/' + code,
                type: 'GET',
                dataType: 'json',
            })
            // on success
            jqXHR.done(function(data, status, xhr) {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('error').addClass('success');
                product.find('.help-block').remove();
                product.removeClass('alert alert-block alert-error persist');

                $(source).closest('.product').find('.name').val(data.name);

                // up amount
                if (!$(product).find('.amount').val()) {
                    $(product).find('.amount').val(1);
                }
            });
            // on error
            jqXHR.fail(function() {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('success').addClass('error');
                product.find('.name').val('');
                if (!product.find('.help-block').length) {
                    product.find('.name').parent().after($('<span>').addClass('help-block').text('Product not found!'));
                }
            });
        }

        $('#code').typeahead({
            source: <?php print json_encode($codes); ?>,
            updater:function (item) {
                //item = selected item

                //do your stuff.
                find_product_by_code($('#code'), item);

                //dont forget to return the item to reflect them into input
                return item;
            }
        }).bind('input change paste keyup mouseup', function() {
            find_product_by_code($(this), $(this).val());
        });

        <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] != 0) { ?>
        function verify_product_by_id(source, id) {
            var jqXHR = $.ajax({
                url: '<?php print option('base_uri'); ?>api/json/product/find/id/' + id,
                type: 'GET',
                dataType: 'json',
            })
            // on success
            jqXHR.done(function(data, status, xhr) {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('error').addClass('success');
                product.find('.help-block').remove();
                product.removeClass('alert alert-block alert-error persist');

                $(source).closest('.product').find('.name').val(data.name);

                // up amount
                if (!$(product).find('.amount').val()) {
                    $(product).find('.amount').val(1);
                }
            });
            // on error
            jqXHR.fail(function() {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('success').addClass('error');
                if (!product.find('.help-block').length) {
                    product.find('.name').parent().after($('<span>').addClass('help-block').text('Product not found!'));
                }
            });
        }

        var list = new Array();
        <?php
        foreach($names as $name) {
            print 'list.push(' . json_encode($name) . ');' . PHP_EOL;
        }
        ?>

        var termTemplate = "<span class='ui-autocomplete-match-highlight'>%s</span>";
        $('#manualmode .name').not(':disabled').autocomplete({
            autoFocus: true,
            minLength: 0,
            source: list,
            position: {
                offset: '0 2' // Shift 0px left, 2px down.
            },
            focus: function(event, ui) {
                $(this).val(ui.item.label);
                return false;

            },
            change: function (event, ui) {
                // Force value must be a match, i.e. select from the dropdown not just typing the name.
                if (!ui.item) {
                     $(this).val('');
                 }
            },
            select: function(event, ui) {
                $(this).val(ui.item.label);
                verify_product_by_id(this, ui.item.value);
                $(this).closest('.product').find('.id').val(ui.item.value);
                return false;
            },
            open: function(event, ui) {
                /* Wrap match in class */
                var data = $(this).data('autocomplete');

                data.menu.element.find('a').each(function() {
                    var me = $(this);
                    var regex = new RegExp(data.term, "gi");
                    me.html( me.text().replace(regex, function (matched) {
                        return termTemplate.replace('%s', matched);
                    }) );
                });
            }
        }).bind('focus', function() {
            $(this).autocomplete('search');
        }).bind('change', function() {
            verify_product_by_id(this, $(this).closest('.product').find('.id').val());
        });
        <?php } ?>
    });
</script>

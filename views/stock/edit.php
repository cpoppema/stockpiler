<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>stock/<?php print $stock['id']; ?>/edit" method="post" class="form-vertical">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="name">Product</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-font"></i></span>
                            <input class="input-xlarge" id="name" name="name" type="text" value="<?php print $stock['product']['name']; ?>" disabled="true" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <label class="control-label" for="amount">Amount</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-exclamation-sign"></i></span>
                            <input class="input-small" id="amount" name="amount" type="number" pattern="\d+" min="1" step="1" value="<?php print $stock['amount']; ?>" />
                        </div>
                    </div>
                </div>
                <br />
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" name="iscanceled" value="1" <?php if ($stock['iscanceled'] == 1) { ?>checked="true"<?php } ?> /> This <?php print $stock['type']; ?> has been canceled.
                        </label>
                    </div>
                </div>
            </fieldset>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save <?php print $stock['type']; ?></button>&nbsp;<button type="reset" class="btn">Cancel</button>
                <a href="<?php print url_for('stock'); ?>" class="btn pull-right">Back</a>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to add products to your existing stock. Make sure that the correct code has been filled in.</p>
            <br />
        </div>
    <?php } ?>
</div>
<div class="row">
    <?php if ($_SESSION['CurrentUser_HidePageDescription']) { ?>
    <div class="span8">
    <?php } else { ?>
    <div class="span6">
    <?php } ?>
        <form action="<?php print option('base_uri'); ?>stock/delivery/bulk" method="post">
            <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0) { ?>
            <div id="handscannermode">
                <?php print $body; ?>
                <input type="hidden" name="manualmode" value="0" />
            </div>
            <?php } else { ?>
            <div id="manualmode">
                <input type="hidden" name="manualmode" value="1" />
                <?php print $body; ?>
            </div>
            <br />
            <a href="#" id="add-product">Add product</a>
            <?php } ?>
            <br />
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Add to stock</button>&nbsp;<button type="reset" class="btn">Cancel</button>
            </div>
        </form>
    </div>
    <?php if ($_SESSION['CurrentUser_HidePageDescription'] == "0") { ?>
        <div class="span2">
            <h5>Page Description</h5>
            <p>This page allows you to add various products at once with a scanner.
            <br />
            <br />Input by keyboard is prevented, but it isn't.</p>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function() {
        function find_product_by_code(source, code) {
            var jqXHR = $.ajax({
                url: '<?php print option('base_uri'); ?>api/json/product/find/code/' + code,
                type: 'GET',
                dataType: 'json',
            })
            // on success
            jqXHR.done(function(data, status, xhr) {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('error').addClass('success');
                product.find('.help-block').remove();
                product.removeClass('alert alert-block alert-error persist');

                $(source).closest('.product').find('.name').val(data.name);

                // up amount
                var input = $(product).find('.amount');
                var value = parseInt(input.val() | 0);
                input.val(value + 1);
            });
            // on error
            jqXHR.fail(function() {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('success').addClass('error');
                if (!product.find('.help-block').length) {
                    product.find('.name').parent().after($('<span>').addClass('help-block').text('Product not found!'));
                }
            });
        }


        <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0) { ?>
        var template = $('<hr>').before($('#product-0').clone());
        <?php } else { ?>
        var template = $('<hr>').after($('#product-0').clone());
        <?php } ?>
        $(template).find(':input').each(function() {
            $(this).removeAttr('value');
        });

        $('form').on('close', '.product', function() {
            if ($('.product').length == 1) {
                new_product = template.clone();
                $(new_product).hide();
                <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0) { ?>
                $('form > div:first').prepend(new_product);
                <?php } else { ?>
                $('form > div:first').append(new_product);
                <?php } ?>
                $(new_product).prev('hr').remove();
                $(new_product).find('close').alert();
                $(new_product).slideDown(function() {
                    $(this).find('.code').focus();
                });
                new_product = null;
            } else {
                if ($(this)[0] == $('.product:first')[0]) {
                    $(this).closest('.product').next('hr').hide().remove();
                } else {
                    $(this).closest('.product').prev('hr').hide().remove();
                }
            }
        });

        var index = 1;
        var input_buffer = '';
        $('form').on('paste change', '.code', function(event) { event.preventDefault(); });
        $('form').on('keypress', '.code', function(event) {
            if (event.which == 32 && input_buffer.length > 0) {
                // get product
                var product = $(this).closest('.product');

                // find product for code
                if ($(this).val() != input_buffer || !$(this).hasClass('legit')) {
                    // find match, first empty or create new
                    var existing_product = $('.product').filter(function(index) { return $(this).find('.code').val() == input_buffer; });
                    if (existing_product.length) {
                        product = existing_product[0];
                    } else {
                        if ($(product).find('.control-group.error').length) {
                            // stay in the current input until a correct code is entered
                        } else {
                            var empty_products = $('.product').filter(function(index) { return $(this).find('.code').val() == ''; });
                            if (empty_products.length) {
                                product = empty_products[0];
                            } else {
                                new_product = template.clone();
                                product = new_product[0];
                            }
                        }
                    }

                    // set code
                    $(product).find('.code').val(input_buffer).focus();
                    find_product_by_code($(product).find('.code'), $(product).find('.code').val());

                    // slide out new product if any
                    if (typeof new_product != "undefined") {
                        $(new_product).hide();
                        $('#handscannermode').prepend(new_product);
                        $(product).attr('id', 'product-'+index);
                        $(product).find('.close').attr('data-target', '#product-'+index); index++;
                        $(new_product).slideDown(function() {
                            $(product).find('.code').focus();
                        });
                        new_product = null;
                    }
                } else {
                    // up amount
                    var input = $(product).find('.amount');
                    var value = parseInt(input.val() | 0);
                    input.val(value + 1);
                }

                // start over
                input_buffer = '';
            } else if (event.which != 32) {
                input_buffer += String.fromCharCode(event.charCode);
            }
            event.preventDefault();
        });

        <?php if ($_SESSION['CurrentUser_ManualModeIsDefault'] != 0) { ?>
        $('#add-product').click(function(event) {
            new_product = template.clone();
            // slide out new product if any
            if (typeof new_product != "undefined") {
                $(new_product).hide();
                product = new_product[1];
                $('#manualmode').append(new_product);
                $(product).attr('id', 'product-'+index);
                $(product).find('.close').attr('data-target', '#product-'+index); index++;
                $(new_product).slideDown();
                // scroll down
                $('body').animate({scrollTop: 137 * $('.product').length}, 500);
                new_product = null;
            }
            event.preventDefault();
        });
        function verify_product_by_id(source, id) {
            var jqXHR = $.ajax({
                url: '<?php print option('base_uri'); ?>api/json/product/find/id/' + id,
                type: 'GET',
                dataType: 'json',
            })
            // on success
            jqXHR.done(function(data, status, xhr) {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('error').addClass('success');
                product.find('.help-block').remove();
                product.removeClass('alert alert-block alert-error persist');

                $(source).closest('.product').find('.name').val(data.name);

                // up amount
                if (!$(product).find('.amount').val()) {
                    $(product).find('.amount').val(1);
                }
            });
            // on error
            jqXHR.fail(function() {
                var product = $(source).closest('.product');
                product.find('.control-group').removeClass('success').addClass('error');
                if (!product.find('.help-block').length) {
                    product.find('.name').parent().after($('<span>').addClass('help-block').text('Product not found!'));
                }
            });
        }

        var list = new Array();
        <?php
        foreach($names as $name) {
            print 'list.push(' . json_encode($name) . ');' . PHP_EOL;
        }
        ?>

        var termTemplate = "<span class='ui-autocomplete-match-highlight'>%s</span>";
        $('#manualmode').on('focus', '.name:not(.ui-autocomplete-input):not(:disabled)', function() {
            $(this).autocomplete({
                autoFocus: true,
                minLength: 0,
                source: list,
                position: {
                    offset: '0 2' // Shift 0px left, 2px down.
                },
                focus: function(event, ui) {
                    $(this).val(ui.item.label);
                    return false;

                },
                change: function (event, ui) {
                    // Force value must be a match, i.e. select from the dropdown not just typing the name.
                    if (!ui.item) {
                         $(this).val('');
                     }
                },
                select: function(event, ui) {
                    $(this).val(ui.item.label);
                    verify_product_by_id(this, ui.item.value);
                    $(this).closest('.product').find('.id').val(ui.item.value);
                    return false;
                },
                open: function(event, ui) {
                    /* Wrap match in class */
                    var data = $(this).data('autocomplete');

                    data.menu.element.find('a').each(function() {
                        var me = $(this);
                        var regex = new RegExp(data.term, "gi");
                        me.html( me.text().replace(regex, function (matched) {
                            return termTemplate.replace('%s', matched);
                        }) );
                    });
                }
            }).bind('focus', function() {
                $(this).autocomplete('search');
            }).bind('change', function() {
                verify_product_by_id(this, $(this).closest('.product').find('.id').val());
            });
        });
        <?php } ?>
    });
</script>

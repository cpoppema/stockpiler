<?php

    const MSG_SUCCESS_ADD_ORDER = 'Your order was added successfully!';
    const MSG_SUCCESS_EDIT_ORDER = 'Your order was updated successfully!';

    const MSG_UNAUTHORIZED_ADD_ORDER = 'You are not authorized to add an order!';
    const MSG_UNAUTHORIZED_EDIT_ORDER = 'You are not authorized to edit an order!';


    function orders_history()
    {
        Security_Authorize();

        $orders = R::findAll('order', 'order by id desc');
        $index = R::$adapter->getAffectedRows();

        R::preload($orders, array('product'));

        foreach($orders as $order)
        {

            // Calculate progress
            $progress_current = 0;
            $deliveries = R::find('stock', 'order_id = ? and (iscanceled = 0 or iscanceled is null)', array($order->id));
            foreach($deliveries as $delivery)
            {
                $progress_current += $delivery->amount;
            }
            $progress_total = $order['amount'];

            $progress_perc = 1;
            $progress_class = 'danger';
            if ($progress_current > 0)
            {
                $progress_class = 'warning';
                $progress_perc = floor(($progress_current / $progress_total) * 100);
            }
            elseif ($progress_current == $progress_total)
            {
                $progress_class = 'success';
                $progress_perc = 100;
            }

            if ($progress_class != 'danger')
            {
                if ($progress_current == $progress_total)
                {
                    $progress_bar = "<div class='progress progress-success'><div class='bar' style='width: 100%'>{$progress_total}</div></div>";
                } else {
                    $progress_bar = "<div class='progress'><div class='bar bar-{$progress_class}' style='width: {$progress_perc}%'>{$progress_current}</div><div class='bar bar-danger' style='width: ". (100 - $progress_perc) . "%'>{$progress_total}</div></div>";
                }
            } else {
                $progress_bar = "<div class='progress'><div class='bar bar-success' style='width: 2%'></div><div class='bar bar-danger' style='width: 98%'>0 / {$progress_total}</div></div>";
            }

            $css_class = 'error';
            if ($order['hasarrived'] == 1)
            {
                $css_class = 'success';
            }
            if ($order['iscanceled'] == 1)
            {
                $css_class = 'warning';
            }

            if ($order['iscanceled'] == 1 && $_SESSION['CurrentUser_HideInacitve'])
            {
                $css_class .= ' hide';
            }

            $body .= "<tr class='" . $css_class . "'>\n";

            $body .= "<th>\n";
            $body .= $index;
            $body .= "</th>\n";
            $body .= "<td class='name'>\n";
            $body .= $order['product']['name'];
            $body .= "</td>\n";
            $body .= "<td class='code'>\n";
            $body .= $order['product']['code'];
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= $progress_bar;
            $body .= "</td>\n";

			if ($_SESSION["CurrentUser_IsReadOnly"] != "1")
			{
				$body .= "<td>\n";
				$body .= "<a href='" . option('base_uri') . "orders/" . $order['id'] . "'>Edit</a>\n";
				$body .= "</td>\n";
            }

            $body .= "</tr>\n";

            $index--;
        }

        if (count($orders) == 0)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='5'>You're an order.</td>\n";
            $body .= "</tr>\n";
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::findAll('product', 'order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = $product['name'];
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", "Order History");
        set("body", $body);
        return html("orders/history.php");
    }

    function orders_add()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_ADD_ORDER);
            exit;
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = $product['name'];
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", "New Order");
        return html("orders/add.php");
    }

    function orders_add_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_ADD_ORDER);
            exit;
        }

        if (empty($_POST['name']) || strlen(trim($_POST['name'])) == 0)
        {
            $_GET['error'] = "You need to provide at least the product's name!";
        }
        if ( !isset($_POST['amount']) || $_POST['amount'] <= 0)
        {
            $_GET['error'] = "Care to tell us how much you really want to order?!";
        } else {
            $product = R::findOne('product', 'name = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['name']));
            if (R::$adapter->getAffectedRows() < 1)
            {
                $_GET['error'] = "The name you provided was not found! Did you tried to order a previous deleted product perhaps?";
            } else {
                // Add Order
                $order = R::dispense('order');
                $order->product = $product;
                $order->amount =  $_POST['amount'];
                $order->hasarrived = false;
                $order->iscanceled = false;
                $order->reason = null;
                $id = R::store($order);

                // Log adding order
                $entry = R::dispense('log');
                $entry->action = 'created';
                $entry->object = $order->getID();
                $entry->object_type = $order->getMeta('type');
                $entry->user_id = $_SESSION['CurrentUser_ID'];
                $entry->date = R::isoDateTime();
                R::store($entry);

                // Order added, go to list
                header("Location: " . option('base_uri') . "orders&success=" . MSG_SUCCESS_ADD_ORDER);
                exit;
            }
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = $product['name'];
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);

        // Errors, go to form
        set("title", "New Order");
        return html("orders/add.php");
    }

    function orders_edit()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_ORDER);
            exit;
        }

        $order = R::load('order', params('id'));
        if (!$order->id)
        {
            set("title", "Not Found");
            set("type", "order");
            return html("error/notfound.php");
        }

        // Find partial deliveries
        $deliveries = R::find('stock', 'order_id = ? and (iscanceled = 0 or iscanceled is null) order by id asc', array($order->id));
        $progress_total = $order->amount;

        $index = 1;

        $has_all_products = false;
        $progress_current = 0;
        $partial_deliveries_body = "";
        foreach($deliveries as $delivery)
        {
            $progress_current += $delivery->amount;
            $progress_perc = floor(($progress_current / $progress_total) * 100);

            $partial_deliveries_body .= "<tr>";
            $partial_deliveries_body .= "<th>" . $index . "</th>";
            $partial_deliveries_body .= "<td>&plus;" . $delivery->amount . "</td>";

            $partial_deliveries_body .=  "<td>";

            if ($progress_current == $progress_total)
            {
                $has_all_products = true;
                $partial_deliveries_body .= "<div class='progress progress-success'><div class='bar' style='width: 100%'>{$progress_total}</div></div>";
            } else {
                $partial_deliveries_body .= "<div class='progress'><div class='bar bar-warning' style='width: {$progress_perc}%'>{$progress_current}</div><div class='bar bar-danger' style='width: " .  (100 - $progress_perc) . "%'>{$progress_total}</div></div>";
            }
            $partial_deliveries_body .= "</td>";
            if ($_SESSION["CurrentUser_IsReadOnly"] != "1")
            {
                $partial_deliveries_body .= "<td style='width: 100px;'>";
                $partial_deliveries_body .= "<a href='" . url_for('stock') . "/" . $delivery->getID() . "'>Edit</a>";
                $partial_deliveries_body .= "</td>";
            }
            $partial_deliveries_body .= "</tr>";

            $index++;
        }

        // Update has_arrived (in light of partial delivery or maybe canceled deliveries)
        $order->hasarrived = $progress_current == $progress_total;
        R::store($order);

        $max_partial_amount = $progress_total - $progress_current;

        R::preload($order, array('product'));
        set("title", "Edit Order");
        set("order", $order);
        set("has_all_products", $has_all_products);
        set("max_partial_amount", $max_partial_amount);
        set("partial_deliveries_body", $partial_deliveries_body);
        return html("orders/edit.php");
    }

    function orders_edit_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_ORDER);
            exit;
        }

        $order = R::load('order', params('id'));
        if (!$order->id)
        {
            set("title", "Not Found");
            set("type", "order");
            return html("error/notfound.php");
        }

        // Log editing order
        $entry = R::dispense('log');
        $entry->action = 'modified';
        if ($_POST['iscanceled'] == 1)
        {
            if ($order->iscanceled != 1)
            {
                $entry->action = 'canceled';
            } else {
                $entry->action = 'modified when canceled';
            }
        }
        elseif ($order->iscanceled == 1)
        {
            $entry->action = 'uncanceled';
        }

        // Add partial delivery
        if (isset($_POST['partial-delivery']))
        {
            $progress_current = R::getCol('SELECT SUM(amount) as amount FROM stock WHERE order_id = ? and (iscanceled = 0 or iscanceled is null)', array($order->id));
            if ((int) $progress_current[0]['amount'] + (int) $_POST['partial-amount'] > (int) $order->amount)
            {
                header("Location: " . option('base_uri') . "orders/" . $order->getID() . "&error=You cannot add more than you've initially ordered.");
                exit;
            }

            // Add Partal Delivery
            $stock = R::dispense('stock');
            R::preload($order, array('product'));
            $stock->product = R::load('product', $order->product->getID());
            $stock->type = 'delivery';
            $stock->amount =  $_POST['partial-amount'];
            $stock->reason = null;
            $stock->iscanceled = false;
            $stock->order = $order;
            $id = R::store($stock);

            // Log editing order
            $entry = R::dispense('log');
            $entry->action = 'modified';
            $entry->object = $order->getID();
            $entry->object_type = $order->getMeta('type');
            $entry->user_id = $_SESSION['CurrentUser_ID'];
            $entry->date = R::isoDateTime();
            R::store($entry);

            // Log adding delivery
            $new_entry = R::dispense('log');
            $new_entry->action = 'created';
            $new_entry->object = $stock->getID();
            $new_entry->object_type = $stock->getMeta('type');
            $new_entry->user_id = $_SESSION['CurrentUser_ID'];
            // Use same timestamp
            $new_entry->date = $entry->date;
            R::store($new_entry);

            header("Location: " . option('base_uri') . "orders/" . $order->getID());
            exit;
        } else {
            // Edit order
            $order->amount = $_POST['amount'];
            $order->hasarrived = (int) $_POST['hasarrived'];
            $order->iscanceled = (int) $_POST['iscanceled'];
            R::store($order);

            // Continue log entry editing order
            $entry->object = $order->getID();
            $entry->object_type = $order->getMeta('type');
            $entry->user_id = $_SESSION['CurrentUser_ID'];
            $entry->date = R::isoDateTime();
            R::store($entry);

            // Check if order is marked as arrived
            if ($order->hasarrived == 1)
            {
                // Calculate amount of products delivered
                $progress_current = R::getCol('SELECT SUM(amount) as amount FROM stock WHERE order_id = ? and (iscanceled = 0 or iscanceled is null)', array($order->id));
                $amount = (int) $order->amount - (int) $progress_current[0]['amount'];

                // Add Delivery
                $stock = R::dispense('stock');
                R::preload($order, array('product'));
                $stock->product = R::load('product', $order->product->getID());
                $stock->type = 'delivery';
                $stock->amount =  $amount;
                $stock->reason = null;
                $stock->iscanceled = false;
                $stock->order = $order;
                $id = R::store($stock);

                // Log adding delivery
                $new_entry = R::dispense('log');
                $new_entry->action = 'created';
                $new_entry->object = $stock->getID();
                $new_entry->object_type = $stock->getMeta('type');
                $new_entry->user_id = $_SESSION['CurrentUser_ID'];
                // Use same timestamp
                $new_entry->date = $entry->date;
                R::store($new_entry);
            }

            header("Location: " . option('base_uri') . "orders/&success=" . MSG_SUCCESS_EDIT_ORDER);
            exit;
        }
    }

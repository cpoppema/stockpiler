<?php

    const MSG_SUCCESS_ADD_PRODUCT = 'Your product was added successfully!';
    const MSG_SUCCESS_EDIT_PRODUCT = 'Your product was updated successfully!';

    const MSG_UNAUTHORIZED_ADD_PRODUCT = 'You are not authorized to add a product!';
    const MSG_UNAUTHORIZED_EDIT_PRODUCT = 'You are not authorized to edit a product!';


	function products_list()
	{
        Security_Authorize();

        $names = array();
        $codes = array();

        $products = R::$f->begin()
            ->select('product.*, ')
                ->open()
                    ->select('SUM(amount)')->from('`order`')->where('`order`.product_id = product.id')->and('(iscanceled = 0 or iscanceled is null)')->and('(hasarrived = 0 or hasarrived is null)')
                ->close()->as('expectedstock,')
                ->open()
                    ->select('SUM(amount)')->from('stock')->where('stock.product_id = product.id')->and('iscanceled = 0 or iscanceled is null')
                ->close()->as('amount')
            ->from('product')
            ->order_by('name asc, code asc')
            ->get();

        foreach($products as $product)
        {
            // Add typeahead data
            $names[] = $product['name'];
            $codes[] = $product['code'];

            $css_class = 'success';
            if ($product['amount'] < $product['minimumstock'])
            {
                $css_class = 'error';
            }

            if ($product['isdeleted'])
            {
                $css_class .= ' warning deleted';
            }

            if ($product['isdeleted'] && $_SESSION['CurrentUser_HideInacitve'])
            {
                $css_class .= ' hide';
            }

            $body .= "<tr class='{$css_class}'>\n";

            $body .= "<th class='name'>\n";
            $body .= $product['name'];
            $body .= "</th>\n";
            $body .= "<td class='code'>\n";
            $body .= $product['code'];
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= $product['amount'] + $product['expectedstock'];
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= $product['amount'];
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= $product['minimumstock'];
            $body .= "</td>\n";

			if ($_SESSION["CurrentUser_IsReadOnly"] != "1")
			{
				$body .= "<td>\n";
				$body .= "<a href='" . option('base_uri') . "products/" . $product['id'] . "'>Edit</a>\n";
				$body .= "</td>\n";
			}

            $body .= "</tr>\n";
        }

        if (R::$adapter->getAffectedRows() < 1)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='6'>You're a product.</td>\n";
            $body .= "</tr>\n";
        }

        // Set typeahead data
        set("names", $names);
        set("codes", $codes);
        set("title", "Products");
        set("body", $body);
        return html("products/list.php");
	}

	function products_add()
	{
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_ADD_PRODUCT);
            exit;
        }

        set("title", "New Product");
        return html("products/add.php");
	}

	function products_add_post()
	{
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_ADD_PRODUCT);
            exit;
        }

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            header("Location: " . option('base_uri') . "products&error=" . MSG_UNAUTHORIZED_ADD_PRODUCT);
            exit;
        }

        $testproduct = R::findOne('product', 'name = ? or code = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['name'], $_POST['code']));
        if ($testproduct)
        {
            if ($testproduct['name'] == $_POST['name'])
            {
                $_GET['error'] = 'There already exists a product with the name "' . $testproduct['name'] . '"';
            }

            if ($testproduct['code'] == $_POST['code'])
            {
                $_GET['error'] = 'The code you provided is already in use for product "' . $testproduct['name'] . '"!';
            }

            // Product name or code in use, go to form
            set("title", "New Product");
            return html("products/add.php");
        }

        // Add product
        $product = R::dispense('product');
        $product->name = $_POST['name'];
        $product->code = $_POST['code'];
        $product->minimumstock = $_POST['minimumstock'];
        $product->isdeleted = (int) false;
        $id = R::store($product);

        // Log adding product
        $entry = R::dispense('log');
        $entry->action = 'created';
        $entry->object = $product->getID();
        $entry->object_type = $product->getMeta('type');
        $entry->user_id = $_SESSION['CurrentUser_ID'];
        $entry->date = R::isoDateTime();
        R::store($entry);

        // Product added, go to list
        header("Location: " . option('base_uri') . "products&success=" . MSG_SUCCESS_ADD_PRODUCT);
        exit;
	}

	function products_edit()
	{
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_PRODUCT);
            exit;
        }

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0"
            && $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "products&error=" . MSG_UNAUTHORIZED_EDIT_PRODUCT);
            exit;
        }

        $product = R::load('product', params('id'));
        if (!$product->id)
        {
            set("title", "Product Not Found");
            set("type", "product");
            return html("error/notfound.php");
        }

        set("title", "Edit Product");
        set("product", $product);
        return html("products/edit.php");
	}

	function products_edit_post()
	{
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_PRODUCT);
            exit;
        }

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0" &&
            $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "products&error=" . MSG_UNAUTHORIZED_EDIT_PRODUCT);
            exit;
        }

        $product = R::load('product', params('id'));
        if (!$product->id)
        {
            set("title", "Product Not Found");
            set("type", "product");
            return html("error/notfound.php");
        }

        $testproduct = R::findOne('product', '(name = ? or code = ?) and id != ?', array($_POST['name'], $_POST['code'], params('id')));
        if ($testproduct)
        {
            if ($testproduct['name'] == $_POST['name'])
            {
                $_GET['error'] = 'There already exists a product with the name "' . $testproduct['name'] . '"';
            }

            if ($testproduct['code'] == $_POST['code'] && !$testproduct->isdeleted)
            {
                $_GET['error'] = 'The code you provided is already in use for product "' . $testproduct['name'] . '"!';
            }

            if (empty($_POST['isdeleted']) && !empty($product['isdeleted']) && !$testproduct->isdeleted)
            {
                $_GET['error'] = 'Unable to restore this product because the code is now in use for a different product. Change the code or delete the product "' . $testproduct['name'] . '"';
            }

            // Product name or code in use, go to form
            set("title", "Edit Product");
            set("product", $product);
            return html("products/edit.php");
        }

        // Log editing product
        $entry = R::dispense('log');
        $entry->action = 'modified';
        if ($_POST['isdeleted'] == 1)
        {
            if ($product->isdeleted != 1)
            {
                $entry->action = 'deleted';
            } else {
                $entry->action = 'modified when deleted';
            }
        }
        elseif ($product->isdeleted == 1)
        {
            $entry->action = 'undeleted';
        }

        // Edit product
        $product->name = $_POST['name'];
        $product->code = $_POST['code'];
        $product->minimumstock = $_POST['minimumstock'];
        $product->isdeleted = (int) $_POST['isdeleted'];
        R::store($product);

        // Continue log entry editing product
        $entry->object = $product->getID();
        $entry->object_type = $product->getMeta('type');
        $entry->user_id = $_SESSION['CurrentUser_ID'];
        $entry->date = R::isoDateTime();
        R::store($entry);

        header("Location: " . option('base_uri') . "products/&success=" . MSG_SUCCESS_EDIT_PRODUCT);
        exit;
	}

<?php

    const MSG_SUCCESS_BULK_DELIVERY = 'Your products have been added to stock successfully!';
    const MSG_SUCCESS_BULK_PICKUP = 'Your products have been removed from stock successfully!';

    const MSG_UNAUTHORIZED_BULK_DELIVERY = 'You are not authorized to make bulk deliveries!';
    const MSG_UNAUTHORIZED_BULK_PICKUP = 'You are not authorized to do bulk pickups!';

    function stock_bulk_delivery()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_BULK_DELIVERY);
            exit;
        }

        if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0)
        {
            $body = ''.
            '<fieldset id="product-0" class="product fade in">'.
                '<a class="close" data-dismiss="alert" data-target="#product-0" href="#">&times;</a>'.
                '<div class="control-group">'.
                    '<label class="control-label legit" for="code">Code</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-barcode"></i></span>'.
                            '<input class="input-xlarge code" id="code" name="code[]" type="text" autofocus="autofocus" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="name">Name</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-font"></i></span>'.
                            '<input class="input-xlarge name" id="name" name="name[]" type="text" disabled="disabled" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="amount">Amount</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                            '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
            '</fieldset>';
        } else {
            $body = ''.
            '<fieldset id="product-0" class="product fade in">'.
                '<a class="close" data-dismiss="alert" data-target="#product-0" href="#">&times;</a>'.
                '<input type="hidden" name="id[]" class="id" required />'.
                '<div class="control-group">'.
                    '<label class="control-label" for="name">Name</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-font"></i></span>'.
                            '<input class="input-xlarge name" id="name" name="name[]" type="text" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="amount">Amount</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                            '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
            '</fieldset>';
        }

        $names = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
        }
        set("names", $names);
        set("title", "Stock Bulk Delivery");
        set("body", $body);
        return html("stock/delivery_bulk.php");
    }

    function stock_delivery_bulk_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_BULK_DELIVERY);
            exit;
        }

        $errors = false;

        // Form validation
        $products = array();
        for($i = 0; $i < count($_POST['amount']); $i++)
        {

            if ($_POST['manualmode'] == '1')
            {
                $id = $_POST['id'][$i];
                $amount = $_POST['amount'][$i];
                $name = $_POST['name'][$i];

                $products[$i]['id'] = $id;
                $products[$i]['amount'] = intval($amount);
                $products[$i]['name'] = $name;

                if (strlen(trim($id)) == 0) {
                    $products[$i]['error']['id'] = "Bwoaah..! AAAAH!! Product id missing.";
                    $products[$i]['error']['amount'] = "Need a number!";
                    $products[$i]['error']['name'] = "Nice try.";
                    $errors = true;
                } else {
                    if ($products[$i]['amount'] < 1)
                    {
                        $products[$i]['error']['amount'] = "That's .. not enough!";
                        $errors = true;
                    }

                    $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['id']));
                    if (!$product)
                    {
                        $products[$i]['error']['name'] = "Product not found!<br />Did you try to scan a previous deleted product perhaps?";
                        $errors = true;
                    }
                }
            } else {
                $code = $_POST['code'][$i];
                $amount = $_POST['amount'][$i];

                $products[$i]['code'] = $code;
                $products[$i]['amount'] = intval($amount);
                $products[$i]['name'] = '';

                if (strlen(trim($code)) == 0) {
                    $products[$i]['error']['code'] = "You're not even trying!";
                    $products[$i]['error']['amount'] = "Need a number!";
                    $products[$i]['error']['name'] = "Yea.. Nope.";
                    $errors = true;
                } else {
                    if ($products[$i]['amount'] < 1)
                    {
                        $products[$i]['error']['amount'] = "That's .. not enough!";
                        $errors = true;
                    }

                    $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['code']));
                    if (!$product)
                    {
                        $products[$i]['error']['name'] = "Product not found!<br />Did you try to scan a previous deleted product perhaps?";
                        $errors = true;
                    } else {
                        $products[$i]['name'] = $product['name'];
                    }
                }
            }
        }

        // Insert if no errors
        if(!$errors)
        {
            for($i = 0; $i < count($products); $i++)
            {

                if ($_POST['manualmode'] == '1')
                {
                    $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['id']));

                } else {
                    $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['code']));
                }

                // Add Delivery
                $stock = R::dispense('stock');
                $stock->product = $product;
                $stock->type = 'delivery';
                $stock->amount = $products[$i]['amount'];
                $stock->reason = null;
                $stock->iscanceled = false;
                $id = R::store($stock);

                // Log adding delivery
                $entry = R::dispense('log');
                $entry->action = 'created';
                $entry->object = $stock->getID();
                $entry->object_type = $stock->getMeta('type');
                $entry->user_id = $_SESSION['CurrentUser_ID'];
                $entry->date = R::isoDateTime();
                R::store($entry);
            }

            // Deliveries added, go to list
            header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_BULK_DELIVERY);
            exit;
        }

        // Postback on errors
        $body = '';
        for($i=0; $i < count($products); $i++)
        {
            $css_class = $css_error_class = 'success';
            $error_code = $error_name = $error_amount = '';
            if(isset($products[$i]['error']))
            {
                $css_class = 'alert alert-block alert-error persist ';
                $css_error_class = ' error';
                $error_id = $products[$i]['error']['id'];
                $error_code = $products[$i]['error']['code'];
                $error_name = $products[$i]['error']['name'];
                $error_amount = $products[$i]['error']['amount'];
            }

            if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0)
            {
                $body .= ''.
                '<fieldset id="product-' . $i . '" class="product ' . $css_class . 'fade in">'.
                    '<a class="close" data-dismiss="alert" data-target="#product-' . $i . '" href="#">&times;</a>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label legit" for="code">Code</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-barcode"></i></span>'.
                                '<input class="input-xlarge code" id="code" name="code[]" type="text" value="' .  $products[$i]['code'] . '" />'.
                            '</div>'.
                            (isset($error_code) ? '<span class="help-block">' . $error_code . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="name">Name</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-font"></i></span>'.
                                '<input class="input-xlarge name" id="name" name="name[]" type="text" disabled="disabled" value="' .  $products[$i]['name'] . '"  />'.
                            '</div>'.
                            (isset($error_name) ? '<span class="help-block">' . $error_name . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="amount">Amount</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                                '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" value="' .  $products[$i]['amount'] . '" />'.
                            '</div>'.
                            (isset($error_amount) ? '<span class="help-block">' . $error_amount . '</span>' : '').
                        '</div>'.
                    '</div>'.
                '</fieldset>';
            } else {
                $body .= ''.
                '<fieldset id="product-' . $i . '" class="product ' . $css_class . 'fade in">'.
                    '<a class="close" data-dismiss="alert" data-target="#product-' . $i . '" href="#">&times;</a>'.
                    '<input type="hidden" name="id[]" class="id" required value="' . $products[$i]['id'] . '" />'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="name">Name</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-font"></i></span>'.
                                '<input class="input-xlarge name" id="name" name="name[]" type="text" value="' . $products[$i]['name'] . '" />'.
                            '</div>'.
                            (isset($error_name) ? '<span class="help-block">' . $error_name . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="amount">Amount</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                                '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" value="' .  $products[$i]['amount'] . '" />'.
                            '</div>'.
                            (isset($error_amount) ? '<span class="help-block">' . $error_amount . '</span>' : '').
                        '</div>'.
                    '</div>'.
                '</fieldset>';
            }

            if( $i < (count($products) - 1) && count($products) > 1)
            {
                $body .= '<hr/>';
            }
        }

        $names = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
        }
        set("names", $names);
        set("title", "Stock Bulk Delivery");
        set("body", $body);
        return html("stock/delivery_bulk.php");
    }

    function stock_bulk_pickup()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_BULK_PICKUP);
            exit;
        }


        if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0)
        {
            $body = ''.
            '<fieldset id="product-0" class="product fade in">'.
                '<a class="close" data-dismiss="alert" data-target="#product-0" href="#">&times;</a>'.
                '<div class="control-group">'.
                    '<label class="control-label legit" for="code">Code</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-barcode"></i></span>'.
                            '<input class="input-xlarge code" id="code" name="code[]" type="text" autofocus="autofocus" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="name">Name</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-font"></i></span>'.
                            '<input class="input-xlarge name" id="name" name="name[]" type="text" disabled="disabled" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="amount">Amount</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                            '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
            '</fieldset>';
        } else {
            $body = ''.
            '<fieldset id="product-0" class="product fade in">'.
                '<a class="close" data-dismiss="alert" data-target="#product-0" href="#">&times;</a>'.
                '<input type="hidden" name="id[]" class="id" required />'.
                '<div class="control-group">'.
                    '<label class="control-label" for="name">Name</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-font"></i></span>'.
                            '<input class="input-xlarge name" id="name" name="name[]" type="text" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
                '<div class="control-group">'.
                    '<label class="control-label" for="amount">Amount</label>'.
                    '<div class="controls">'.
                        '<div class="input-prepend">'.
                            '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                            '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" />'.
                        '</div>'.
                    '</div>'.
                '</div>'.
            '</fieldset>';
        }

        $names = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
        }
        set("names", $names);

        set("title", "Stock Bulk Pickup");
        set("body", $body);
        return html("stock/pickup_bulk.php");
    }

    function stock_pickup_bulk_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_BULK_PICKUP);
            exit;
        }

        $errors = false;

        // Form validation
        $products = array();
        for($i = 0; $i < count($_POST['amount']); $i++)
        {

            if ($_POST['manualmode'] == '1')
            {
                $id = $_POST['id'][$i];
                $amount = $_POST['amount'][$i];
                $name = $_POST['name'][$i];

                $products[$i]['id'] = $id;
                $products[$i]['amount'] = intval($amount);
                $products[$i]['name'] = $name;

                if (strlen(trim($id)) == 0) {
                    $products[$i]['error']['id'] = "Bwoaah..! AAAAH!! Product id missing.";
                    $products[$i]['error']['amount'] = "Need a number!";
                    $products[$i]['error']['name'] = "Nice try.";
                    $errors = true;
                } else {
                    if ($products[$i]['amount'] < 1)
                    {
                        $products[$i]['error']['amount'] = "That's .. not enough!";
                        $errors = true;
                    }

                    $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['id']));
                    if (!$product)
                    {
                        $products[$i]['error']['name'] = "Product not found!<br />Did you try to scan a previous deleted product perhaps?";
                        $errors = true;
                    }
                }
            } else {
                $code = $_POST['code'][$i];
                $amount = $_POST['amount'][$i];

                $products[$i]['code'] = $code;
                $products[$i]['amount'] = intval($amount);
                $products[$i]['name'] = '';

                if (strlen(trim($code)) == 0) {
                    $products[$i]['error']['code'] = "You're not even trying!";
                    $products[$i]['error']['name'] = "Yea.. Nope.";
                    $products[$i]['error']['amount'] = "You're a NaN?!";
                    $errors = true;
                } else {
                    if ($products[$i]['amount'] < 1)
                    {
                        $products[$i]['error']['amount'] = "That's .. not enough!";
                        $errors = true;
                    }

                    $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['code']));
                    if (!$product)
                    {
                        $products[$i]['error']['name'] = "Product not found!<br />Did you try to scan a previous deleted product perhaps?";
                        $errors = true;
                    } else {
                        $products[$i]['name'] = $product['name'];
                    }
                }
            }
        }

        // Insert if no errors
        if(!$errors)
        {
            for($i = 0; $i < count($products); $i++)
            {

                if ($_POST['manualmode'] == '1')
                {
                    $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['id']));

                } else {
                    $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($products[$i]['code']));
                }

                // Add Pickup
                $stock = R::dispense('stock');
                $stock->product = $product;
                $stock->type = 'pickup';
                $stock->amount = $products[$i]['amount'] * -1;
                $stock->reason = null;
                $stock->iscanceled = false;
                $id = R::store($stock);

                // Log adding pickup
                $entry = R::dispense('log');
                $entry->action = 'created';
                $entry->object = $stock->getID();
                $entry->object_type = $stock->getMeta('type');
                $entry->user_id = $_SESSION['CurrentUser_ID'];
                $entry->date = R::isoDateTime();
                R::store($entry);
            }

            // Pickups added, go to list
            header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_BULK_PICKUP);
            exit;
        }

        // Postback on errors
        $body = '';
        for($i=0; $i < count($products); $i++)
        {
            $css_class = $css_error_class = 'success';
            $error_code = $error_name = $error_amount = '';
            if(isset($products[$i]['error']))
            {
                $css_class = 'alert alert-block alert-error persist ';
                $css_error_class = ' error';
                $error_code = $products[$i]['error']['code'];
                $error_name = $products[$i]['error']['name'];
                $error_amount = $products[$i]['error']['amount'];
            }


            if ($_SESSION['CurrentUser_ManualModeIsDefault'] == 0)
            {
                $body .= ''.
                '<fieldset id="product-' . $i . '" class="product ' . $css_class . 'fade in">'.
                    '<a class="close" data-dismiss="alert" data-target="#product-' . $i . '" href="#">&times;</a>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label legit" for="code">Code</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-barcode"></i></span>'.
                                '<input class="input-xlarge code" id="code" name="code[]" type="text" value="' .  $products[$i]['code'] . '" />'.
                            '</div>'.
                            (isset($error_code) ? '<span class="help-block">' . $error_code . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="name">Name</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-font"></i></span>'.
                                '<input class="input-xlarge name" id="name" name="name[]" type="text" disabled="disabled" value="' .  $products[$i]['name'] . '"  />'.
                            '</div>'.
                            (isset($error_name) ? '<span class="help-block">' . $error_name . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="amount">Amount</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                                '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" value="' .  $products[$i]['amount'] . '" />'.
                            '</div>'.
                            (isset($error_amount) ? '<span class="help-block">' . $error_amount . '</span>' : '').
                        '</div>'.
                    '</div>'.
                '</fieldset>';
            } else {
                $body .= ''.
                '<fieldset id="product-' . $i . '" class="product ' . $css_class . 'fade in">'.
                    '<a class="close" data-dismiss="alert" data-target="#product-' . $i . '" href="#">&times;</a>'.
                    '<input type="hidden" name="id[]" class="id" required value="' . $products[$i]['id'] . '" />'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="name">Name</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-font"></i></span>'.
                                '<input class="input-xlarge name" id="name" name="name[]" type="text" value="' . $products[$i]['name'] . '" />'.
                            '</div>'.
                            (isset($error_name) ? '<span class="help-block">' . $error_name . '</span>' : '').
                        '</div>'.
                    '</div>'.
                    '<div class="control-group ' . (isset($css_error_class) ? $css_error_class : '') . '">'.
                        '<label class="control-label" for="amount">Amount</label>'.
                        '<div class="controls">'.
                            '<div class="input-prepend">'.
                                '<span class="add-on"><i class="icon-exclamation-sign"></i></span>'.
                                '<input class="input-small amount" id="amount" name="amount[]" type="number" pattern="\d+" min="1" step="1" value="' .  $products[$i]['amount'] . '" />'.
                            '</div>'.
                            (isset($error_amount) ? '<span class="help-block">' . $error_amount . '</span>' : '').
                        '</div>'.
                    '</div>'.
                '</fieldset>';
            }

            if( $i < (count($products) - 1) && count($products) > 1)
            {
                $body .= '<hr/>';
            }
        }


        $names = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
        }
        set("names", $names);
        set("title", "Stock Bulk Pickup");
        set("body", $body);
        return html("stock/pickup_bulk.php");
    }

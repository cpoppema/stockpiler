<?php

    const MSG_EMPTY_LIST_USER = 'There are currently no users setup.';
    const MSG_INVALID_EMAIL = 'That email address is invalid!';
    const MSG_USED_EMAIL = 'A user with that email address already exists!';
    const MSG_PASSWORD_MISMATCH = 'Your passwords do not match!';

    const MSG_SUCCESS_ADD_USER = 'Your user was added successfully!';
    const MSG_SUCCESS_EDIT_USER = 'Your user was updated successfully!';
    const MSG_SUCCESS_DELETE_USER = 'Your user was deleted successfully!';
    const MSG_SUCCESS_RECOVER_USER = 'Your user was recovered successfully!';

    const MSG_UNAUTHORIZED_VIEW_USER = 'You are not authorized to view the list of users!';
    const MSG_UNAUTHORIZED_ADD_USER = 'You are not authorized to add a new user!';
    const MSG_UNAUTHORIZED_EDIT_USER = 'You are not authorized to edit that user!';
    const MSG_UNAUTHORIZED_DELETE_USER = 'You are not authorized to delete this user!';
    const MSG_UNAUTHORIZED_RECOVER_USER = 'You are not authorized to recover this user!';


    function users_list()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_VIEW_USER);
            exit;
        }

        $index = 1;


        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            $users = R::find('user', 'isdeleted = 0 or isdeleted = null order by name asc');
        } else {
            $users = R::findAll('user', 'order by name asc');
        }
        foreach($users as $user)
        {
            $body .= "<tr>\n";

            $body .= "<th>\n";
            $body .= $index;
            $body .= "</th>\n";
            $body .= "<td>\n";
            $body .= $user['name'] . ' ' . (( $user['isdeleted'] == 1 ) ? ' (Deleted)' : '');
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= "<a href='" . option('base_uri') . "users/" . $user['id'] . "'>Edit</a>\n";
            $body .= "</td>\n";

            $body .= "</tr>\n";

            $index++;
        }

        if (R::$adapter->getAffectedRows() < 1)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='2'>" . MSG_EMPTY_LIST_USER . "</td>\n";
            $body .= "</tr>\n";
        }

        set("title", "Users");
        set("body", $body);
        return html("users/list.php");
    }

    function users_add()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            header("Location: " . option('base_uri') . "users&error=" . MSG_UNAUTHORIZED_ADD_USER);
            exit;
        }

        set("title", "New User");
        return html("users/add.php");
    }

    function users_add_post()
    {
        require Root . '/library/phpass-0.3/PasswordHash.php';

        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            header("Location: " . option('base_uri') . "users&error=" . MSG_UNAUTHORIZED_ADD_USER);
            exit;
        }

        // Invalid e-mail
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            header("Location: " . option('base_uri') . "users/add&error=" . MSG_INVALID_EMAIL);
            exit;
        }

        // Didn't confirm password
        if ($user['password'] != $user['passwordconfirm'])
        {
            header("Location: " . option('base_uri') . "users/add&error=" . MSG_PASSWORD_MISMATCH);
            exit;
        }

        // Trying to use existing e-mail
        $count = R::count('user', 'email = ?', array($_POST['email']));
        if ($count > 0)
        {
            header("Location: " . option('base_uri') . "users/add&error=" . MSG_USED_EMAIL);
            exit;
        }

        // Add user
        $user = R::dispense('user');
        $user->username = $_POST['username'];
        $user->name = $_POST['name'];
        $user->email = $_POST['email'];
        $user->receivewarningemails = (int) false; // default false, user can change in profile
        $user->hideinactive = (int) true; // default true, user can change in profile
        $user->bulkisdefault = (int) false; // default false, user can change in profile
        $user->manualmodeisdefault = (int) false; // default false, user can change in profile
        $user->hidepagedescription = (int) false; // default false, user can change in profile
        $user->isadministrator = (int) $_POST['isadministrator'];
        $user->isreadonly = (int) $_POST['isreadonly'];
        $user->createddate = R::$f->now();
        $user->isdeleted = (int) false;

        // hash password to the new hashing mechanism
        $hasher = new PasswordHash(8, FALSE);
        $hash = $hasher->HashPassword($_POST['password']);
        $user->hash = True;
        $user->password = $hash;
        R::store($user);

        // User added, go to list
        header("Location: " . option('base_uri') . "users&success=" . MSG_SUCCESS_ADD_USER);
        exit;
    }

    function users_edit()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0" &&
            $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "users&error=" . MSG_UNAUTHORIZED_EDIT_USER);
            exit;
        }

        $user = R::load('user', params('id'));
        if (!$user->id)
        {
            set("title", "User Not Found");
            set("type", "user");
            return html("error/notfound.php");
        }

        set("title", "Edit User");
        set("user", $user);
        return html("users/edit.php");
    }

    function users_edit_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0" &&
            $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "users&error=" . MSG_UNAUTHORIZED_EDIT_USER);
            exit;
        }

        $user = R::load('user', params('id'));
        if (!$user->id)
        {
            set("title", "User Not Found");
            set("type", "user");
            return html("error/notfound.php");
        }

        // Invalid e-mail
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            header("Location: " . option('base_uri') . "users/" . $user['id'] . "&error=" . MSG_INVALID_EMAIL);
            exit;
        }

        // Didn't confirm password
        if ($_POST['newpassword'] != "" &&
            $_POST['newpassword'] != $_POST['newpasswordconfirm'])
        {
            header("Location: " . option('base_uri') . "users/" . $user['id'] . "&error=" . MSG_PASSWORD_MISMATCH);
            exit;
        }

        // Trying to use existing e-mail
        $count = R::count('user', 'email = ? and id != ?', array($_POST['email'], params('id')));
        if ($count > 0)
        {
            header("Location: " . option('base_uri') . "users/" . $user['id'] . "&error=" . MSG_USED_EMAIL);
            exit;
        }

        // Edit user
        $user->username = $_POST['username'];
        $user->name = $_POST['name'];
        $user->email = $_POST['email'];
        $user->receivewarningemails = (int) $_POST['receivewarningemails'];
        $user->hideinactive = (int) $_POST['hideinactive'];
        $user->bulkisdefault = (int) $_POST['bulkisdefault'];
        $user->manualmodeisdefault = (int) $_POST['manualmodeisdefault'];
        $user->hidepagedescription = (int) $_POST['hidepagedescription'];

        if ($_SESSION['CurrentUser_IsAdministrator'] == "1")
        {
            $user->isadministrator = (int) $_POST['isadministrator'];
        }

        if ($_SESSION['CurrentUser_IsAdministrator'] == "1")
        {
            $user->isreadonly = (int) $_POST['isreadonly'];
        }

        if ($_POST['newpassword'] != "" &&
            $_POST['newpassword'] == $_POST['newpasswordconfirm'])
        {
            require Root . '/library/phpass-0.3/PasswordHash.php';

            // hash password to the new hashing mechanism
            $hasher = new PasswordHash(8, FALSE);
            $hash = $hasher->HashPassword($_POST['newpassword']);
            $user->hash = True;
            $user->password = $hash;
        }
        R::store($user);

        if ($_SESSION['CurrentUser_ID'] == params('id'))
        {
            Security_Refresh($user);
        }

        header("Location: " . option('base_uri') . "users/" . $user['id'] . "&success=" . MSG_SUCCESS_EDIT_USER);
        exit;
    }

    function users_delete()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0" &&
            $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "users/" . params('id') . "&error=" . MSG_UNAUTHORIZED_DELETE_USER);
            exit;
        }

        $user = R::load('user', params('id'));
        if (!$user->id)
        {
            set("title", "User Not Found");
            set("type", "user");
            return html("error/notfound.php");
        }

        $user->isdeleted = 1;
        R::store($user);

        header("Location: " . option('base_uri') . "users&success=" . MSG_SUCCESS_DELETE_USER);
        exit;
    }

    function users_recover()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0" &&
            $_SESSION['CurrentUser_ID'] != params('id'))
        {
            header("Location: " . option('base_uri') . "users/" . params('id') . "&error=" . MSG_UNAUTHORIZED_RECOVER_USER);
            exit;
        }

        $user = R::load('user', params('id'));
        if (!$user->id)
        {
            set("title", "User Not Found");
            set("type", "user");
            return html("error/notfound.php");
        }

        $user->isdeleted = 0;
        R::store($user);

        header("Location: " . option('base_uri') . "users&success=" . MSG_SUCCESS_RECOVER_USER);
        exit;
    }

?>
<?php

    const MSG_SUCCESS_DELIVERY = 'Your products have been added to stock successfully!';
    const MSG_SUCCESS_PICKUP = 'Your products have been removed from stock successfully!';
    const MSG_SUCCESS_EDIT_STOCK = 'Your %s was updated successfully!';

    const MSG_UNAUTHORIZED_DELIVERY = 'You are not authorized to make a delivery!';
    const MSG_UNAUTHORIZED_PICKUP = 'You are not authorized to do a pickup!';
    const MSG_UNAUTHORIZED_EDIT_STOCK = 'You are not authorized to edit deliveries and pickups!';


    function stock_history()
    {
        Security_Authorize();

        $stock_changes = R::findAll('stock', 'order by id desc');
        $index = R::$adapter->getAffectedRows();

        R::preload($stock_changes, array('product'));

        foreach($stock_changes as $stock)
        {
            $css_class = ($stock['type'] == 'delivery') ? 'success' : 'error';
            if ($stock['iscanceled'] == 1)
            {
                $css_class .= ' warning';
            }

            if ($stock['iscanceled'] == 1 && $_SESSION['CurrentUser_HideInacitve'])
            {
                $css_class .= ' hide';
            }

            $body .= "<tr class='" . $css_class . "'>\n";

            $body .= "<th>\n";
            $body .= $index;
            $body .= "</th>\n";
            $body .= "<td class='name'>\n";
            $body .= $stock['product']['name'];
            $body .= "</td>\n";
            $body .= "<td class='code'>\n";
            $body .= $stock['product']['code'];
            $body .= "</td>\n";
            $body .= "<td>\n";
            $body .= (($stock['type'] == 'delivery') ? '&plus;' : '&minus;') . '&nbsp;' . abs($stock['amount']);
            $body .= "</td>\n";
			if ($_SESSION["CurrentUser_IsReadOnly"] != "1")
			{
				$body .= "<td>\n";
				$body .= "<a href='" . option('base_uri') . "stock/" . $stock['id'] . "'>Edit</a>\n";
                if (isset($stock['order_id']))
                {
                    $body .= "| <a href='" . option('base_uri') . "orders/" . $stock['order_id'] . "'>View order</a>\n";
                }
				$body .= "</td>\n";
            } else {
                $body .= "<td>\n";
                if (isset($stock['order_id']))
                {
                    $body .= "<a href='" . option('base_uri') . "stock/" . $stock['order_id'] . "'>View order</a>\n";
                }
                $body .= "</td>\n";
            }

            $body .= "</tr>\n";

            $index--;
        }

        if (R::$adapter->getAffectedRows() < 1)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='3'>You're a stock.</td>\n";
            $body .= "</tr>\n";
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::findAll('product', 'order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = $product['name'];
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", "Stock History");
        set("body", $body);
        return html("stock/history.php");
    }

    function stock_delivery()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_DELIVERY);
            exit;
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", "Stock Delivery");
        return html("stock/delivery.php");
    }

    function stock_delivery_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_DELIVERY);
            exit;
        }

        if ($_POST['manualmode'] == '1')
        {

            if (empty($_POST['id']) || strlen(trim($_POST['id'])) == 0)
            {
                $_GET['error'] = "You need to provide at least a product!";
            }
            if ( !isset($_POST['amount']) || $_POST['amount'] <= 0)
            {
                $_GET['error'] = "Care to tell us how much you really want to add to stock?!";
            } else {
                $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['id']));
                if (R::$adapter->getAffectedRows() < 1)
                {
                    $_GET['error'] = "The product you entered was not found! Did you scan a previous deleted product perhaps?";
                } else {
                    // Add Delivery
                    $stock = R::dispense('stock');
                    $stock->product = $product;
                    $stock->type = 'delivery';
                    $stock->amount =  $_POST['amount'];
                    $stock->reason = null;
                    $stock->iscanceled = false;
                    $id = R::store($stock);

                    // Log adding delivery
                    $entry = R::dispense('log');
                    $entry->action = 'created';
                    $entry->object = $stock->getID();
                    $entry->object_type = $stock->getMeta('type');
                    $entry->user_id = $_SESSION['CurrentUser_ID'];
                    $entry->date = R::isoDateTime();
                    R::store($entry);

                    // Delivery added, go to list
                    header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_DELIVERY);
                    exit;
                }
            }
        } else {
            if (empty($_POST['code']) || strlen(trim($_POST['code'])) == 0)
            {
                $_GET['error'] = "You need to provide at least the product's code!";
            }
            if ( !isset($_POST['amount']) || $_POST['amount'] <= 0)
            {
                $_GET['error'] = "Care to tell us how much you really want to add to stock?!";
            } else {
                $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['code']));
                if (R::$adapter->getAffectedRows() < 1)
                {
                    $_GET['error'] = "The code you entered was not found! Did you scan a previous deleted product perhaps?";
                } else {
                    // Add Delivery
                    $stock = R::dispense('stock');
                    $stock->product = $product;
                    $stock->type = 'delivery';
                    $stock->amount =  $_POST['amount'];
                    $stock->reason = null;
                    $stock->iscanceled = false;
                    $id = R::store($stock);

                    // Log adding delivery
                    $entry = R::dispense('log');
                    $entry->action = 'created';
                    $entry->object = $stock->getID();
                    $entry->object_type = $stock->getMeta('type');
                    $entry->user_id = $_SESSION['CurrentUser_ID'];
                    $entry->date = R::isoDateTime();
                    R::store($entry);

                    // Delivery added, go to list
                    header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_DELIVERY);
                    exit;
                }
            }
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);

        // Errors, go to form
        set("title", "Stock Delivery");
        return html("stock/delivery.php");
    }

    function stock_pickup()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_PICKUP);
            exit;
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", "Stock Pickup");
        return html("stock/pickup.php");
    }

    function stock_pickup_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_PICKUP);
            exit;
        }

        if ($_POST['manualmode'] == '1')
        {

            if (empty($_POST['id']) || strlen(trim($_POST['id'])) == 0)
            {
                $_GET['error'] = "You need to provide at least a product!";
            }
            if ( !isset($_POST['amount']) || $_POST['amount'] <= 0)
            {
                $_GET['error'] = "Care to tell us how much you really want to remove from stock?!";
            } else {
                $product = R::findOne('product', 'id = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['id']));
                if (R::$adapter->getAffectedRows() < 1)
                {
                    $_GET['error'] = "The product you entered was not found! Did you scan a previous deleted product perhaps?";
                } else {
                    // Add Pickup
                    $stock = R::dispense('stock');
                    $stock->product = $product;
                    $stock->type = 'pickup';
                    $stock->amount =  $_POST['amount'] * -1;
                    $stock->reason = null;
                    $stock->iscanceled = false;
                    $id = R::store($stock);

                    // Log adding pickup
                    $entry = R::dispense('log');
                    $entry->action = 'created';
                    $entry->object = $stock->getID();
                    $entry->object_type = $stock->getMeta('type');
                    $entry->user_id = $_SESSION['CurrentUser_ID'];
                    $entry->date = R::isoDateTime();
                    R::store($entry);

                    // Delivery added, go to list
                    header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_PICKUP);
                    exit;
                }
            }
        } else {
            if (empty($_POST['code']) || strlen(trim($_POST['code'])) == 0)
            {
                $_GET['error'] = "You need to provide at least the code!";
            }
            if ( !isset($_POST['amount']) || $_POST['amount'] <= 0)
            {
                $_GET['error'] = "Care to tell us how much you really want to remove from stock?!";
            } else {
                $product = R::findOne('product', 'code = ? and (isdeleted = 0 or isdeleted is null)', array($_POST['code']));
                if (R::$adapter->getAffectedRows() < 1)
                {
                    $_GET['error'] = "The code you entered was not found! Did you scan a previous deleted product perhaps?";
                } else {
                    // Add Pickup
                    $stock = R::dispense('stock');
                    $stock->product = $product;
                    $stock->type = 'pickup';
                    $stock->amount =  $_POST['amount'] * -1;
                    $stock->reason = null;
                    $stock->iscanceled = false;
                    $id = R::store($stock);

                    // Log adding pickup
                    $entry = R::dispense('log');
                    $entry->action = 'created';
                    $entry->object = $stock->getID();
                    $entry->object_type = $stock->getMeta('type');
                    $entry->user_id = $_SESSION['CurrentUser_ID'];
                    $entry->date = R::isoDateTime();
                    R::store($entry);

                    // Pickup added, go to list
                    header("Location: " . option('base_uri') . "stock&success=" . MSG_SUCCESS_PICKUP);
                    exit;
                }
            }
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::find('product', 'isdeleted = 0 or isdeleted is null order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = array('value' => $product['id'], 'label' => $product['name']);  // nested array to allow exact match
            $codes[] = $product['code'];
        }
        set("names", $names);
        set("codes", $codes);

        // Errors, go to form
        set("title", "Stock Pickup");
        return html("stock/pickup.php");
    }

    function stock_edit()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_STOCK);
            exit;
        }

        $stock = R::load('stock', params('id'));
        if (!$stock->id)
        {
            set("title", "Not Found");
            set("type", "");
            return html("error/notfound.php");
        }

        R::preload($stock, array('product'));
        $stock['amount'] = abs($stock['amount']);
        set("title", "Edit " . ucfirst($stock['type']));
        set("stock", $stock);
        return html("stock/edit.php");
    }

    function stock_edit_post()
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsReadOnly'] == "1")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_EDIT_STOCK);
            exit;
        }

        $stock = R::load('stock', params('id'));
        if (!$stock->id)
        {
            set("title", "Not Found");
            set("type", "");
            return html("error/notfound.php");
        }

        // Log editing delivery/pickup
        $entry = R::dispense('log');
        $entry->action = 'modified';
        if ($_POST['iscanceled'] == 1)
        {
            if ($stock->iscanceled != 1)
            {
                $entry->action = 'canceled';
            } else {
                $entry->action = 'modified when canceled';
            }
        }
        elseif ($stock->iscanceled == 1)
        {
            $entry->action = 'uncanceled';
        }

        // Edit delivery/pickup
        $stock->amount = ($stock->type == 'delivery') ? $_POST['amount'] : $_POST['amount'] * -1;
        $stock->iscanceled = ((isset($_POST['iscanceled'])) ? true : false);
        R::store($stock);

        // Continue log entry editing delivery/pickup
        $entry->object = $stock->getID();
        $entry->object_type = $stock->getMeta('type');
        $entry->user_id = $_SESSION['CurrentUser_ID'];
        $entry->date = R::isoDateTime();
        R::store($entry);

        header("Location: " . option('base_uri') . "stock&success=" . sprintf(MSG_SUCCESS_EDIT_STOCK, $stock['type']));
        exit;
    }

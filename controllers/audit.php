<?php

    const MSG_UNAUTHORIZED_VIEW_AUDIT = 'You are not authorized to view the audit trail!';


    function get_trails($filter_type, $filter_id)
    {
        // Filter on either nothing, a product or a user

        // do product
        $product_trails_query = R::$f->begin()
            ->select('log.date, user.name as users_name, log.action, log.object_type, product.name as products_name, log.id, user.id as users_id, product.id as products_id, log.object_type, product.isdeleted, product.code as products_code')
            ->from('log')
                ->join('product on product.id = log.object')
                ->join('user on user.id = log.user_id')
            ->where('log.object_type = ?')->put('product');

        if($filter_type && $filter_id)
        {
            $product_trails_query = $product_trails_query
                ->and($filter_type . '.id = ?')->put($filter_id);
        }

        $product_trails = $product_trails_query
            ->order_by('log.id asc')
            ->get();

        // do stock
        $stock_trails_query = R::$f->begin()
            ->select('log.date, user.name as users_name, log.action, log.object_type, stock.type, stock.amount, product.name as products_name, log.id, user.id as users_id, product.id as products_id, log.object_type, stock.iscanceled, product.isdeleted, product.code as products_code')
            ->from('log')
                ->join('stock on stock.id = log.object')
                ->join('user on user.id = log.user_id')
                ->join('product on product.id = stock.product_id')
            ->where('log.object_type = ?')->put('stock');

        if($filter_type && $filter_id)
        {
            $stock_trails_query = $stock_trails_query
                ->and($filter_type . '.id = ?')->put($filter_id);
        }

        $stock_trails = $stock_trails_query
            ->order_by('log.id asc')
            ->get();

        // build trail display
        $trails = array();
        foreach($product_trails as $trail)
        {
            if ($trail['users_id'] == $_SESSION['CurrentUser_ID'])
            {
                $user = 'You';
            } else {
                $user = $trail['users_name'];
            }
            $params = array(
                '<a href="' . option('base_uri') . 'audit/user/' . $trail['users_id'] . '">' . $user . '</a>',
                (($trail['action'] == 'created')  ? 'added' : 'modified'),
                '<a href="' . option('base_uri') . 'audit/product/' . $trail['products_id'] . '">' . $trail['products_name'] . '</a>',
            );
            $trails[$trail['id']] = array(
                'data-filter' => (($user == 'You') ? 'me' : 'not-me'),
                'css_class' => (( $trail['action'] == 'created' ) ? 'success' : (($trail['action'] == 'modified')  ? 'warning' : 'error')),
                'date' => $trail['date'],
                'deed' => sprintf('%s<br /> %s<br /> product %s', $params[0], $params[1], $params[2]),
                'inactive' => (($trail['isdeleted'] == 1) ? 1 : 0),
                'code' => $trail['products_code']
            );
        }
        foreach($stock_trails as $trail)
        {
            if ($trail['users_id'] == $_SESSION['CurrentUser_ID'])
            {
                $user = 'You';
            } else {
                $user = $trail['users_name'];
            }
            $params = array(
                '<a href="' . option('base_uri') . 'audit/user/' . $trail['users_id'] . '">' . $user . '</a>',
		(($trail['action'] == 'modified') ? 'modified' : (($trail['type'] == 'delivery') ? 'increased' : 'decreased')),
                '<a href="' . option('base_uri') . 'audit/product/' . $trail['products_id'] . '">' . $trail['products_name'] . '</a>',
                abs($trail['amount'])
            );
            $trails[$trail['id']] = array(
                'data-filter' => (($user == 'You') ? 'me' : 'not-me'),
                'css_class' => (($trail['type'] == 'delivery') ? 'success' : 'error'),
                'date' => $trail['date'],
                'deed' => (($params[1] == 'modified') ? sprintf('%s<br /> %s stock for %s<br /> to %s', $params[0], $params[1], $params[2], $params[3]) : sprintf('%s<br /> %s stock for %s<br /> by %s', $params[0], $params[1], $params[2], $params[3])),
                'inactive' => (($trail['iscanceled'] == 1 || $trail['isdeleted'] == 1) ? 1 : 0),
                'code' => $trail['products_code']
            );
        }

        return $trails;
    }

    function get_audit_trail($filter_type, $filter_id)
    {
        Security_Authorize();

        if ($_SESSION['CurrentUser_IsAdministrator'] == "0")
        {
            header("Location: " . option('base_uri') . "&error=" . MSG_UNAUTHORIZED_VIEW_AUDIT);
            exit;
        }

        // Preload audit types for 'product' and 'stock'

        $trails = get_trails($filter_type, $filter_id);

        foreach(array_reverse($trails) as $trail)
        {
            $css_class = $trail['css_class'];
            if ($trail['inactive'] == 1 && $_SESSION['CurrentUser_HideInacitve'])
            {
                $css_class .= ' hide';
            }

            $body .= "<tr class='" . $css_class . "' data-filter='" . $trail['data-filter'] . "'>\n";

            $body .= "<td>\n";
            $body .= $trail['date'];
            $body .= "</td>\n";
            $body .= "<td class='text code' data-code='" . $trail['code'] . "'>\n";
            $body .= $trail['deed'];
            $body .= "</td>\n";

            $body .= "</tr>\n";
        }

        if (count($trails) < 1)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='3'>Nothing to look at here.</td>\n";
            $body .= "</tr>\n";
        }

        // Get typeahead data
        $names = array();
        $codes = array();
        $products = R::findAll('product', 'order by name asc, code asc');
        foreach ($products as $product) {
            $names[] = $product['name'];
            $codes[] = $product['code'];
        }
        $title = "Audit trail";
        if ($filter_type && $filter_id)
        {
            $filter_object = R::load($filter_type, $filter_id);
            $title .= " for " . $filter_type . " " . $filter_object['name'];
        }
        set("names", $names);
        set("codes", $codes);
        set("title", $title);
        set("body", $body);
        return html("audit/trail.php");
    }

    function audit_trail()
    {
        return get_audit_trail();
    }

    function product_audit_trail()
    {
        return get_audit_trail('product', params('id'));
    }

    function user_audit_trail()
    {
        return get_audit_trail('user', params('id'));
    }
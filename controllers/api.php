<?php

    function api_product_table()
    {
        if (!is_valid_api_hash(__FUNCTION__, params('hash')))
        {
            halt(NOT_FOUND);
        }

        $names = array();

        $products = R::$f->begin()
            ->select('product.*, ')
                ->open()
                    ->select('SUM(amount)')->from('stock')->where('stock.product_id = product.id')->and('iscanceled = 0 or iscanceled is null')
                ->close()->as('amount')
            ->from('product')
            ->order_by('name asc, code asc')
            ->get();

        foreach($products as $product)
        {
            // Add typeahead data
            $names[] = $product['name'];
            $css_class = 'success';
            if ($product['amount'] == 0)
            {
                $css_class = 'error';
            }
            elseif ($product['amount'] < $product['minimumstock'])
            {
                $css_class = 'warning';
            }

            if ($product['isdeleted'])
            {
                continue;
            }

            $body .= "<tr class='{$css_class}'>\n";

            $body .= "<th class='name'>\n";
            $body .= $product['name'];
            $body .= "</th>\n";
            $body .= "<td>\n";
            $body .= $product['amount'];
            $body .= "</td>\n";

            $body .= "</tr>\n";
        }

        if (R::$adapter->getAffectedRows() < 1)
        {
            $body .= "<tr>\n";
            $body .= "<td colspan='6'>No products in system.</td>\n";
            $body .= "</tr>\n";
        }

        // Set typeahead data
        set("names", $names);
        set("title", "Search on product name");
        set("body", $body);
		return html("api/product_table.php", "layout.bare.php");
    }

    function api_product_find_by_code_json()
    {
        if ($_SESSION['CurrentUser_ID'] == null)
        {
            halt(NOT_FOUND);
        }

        $product = R::findOne('product', 'code = ?', array(params('code')));
        if (R::$adapter->getAffectedRows() < 1)
        {
            halt(NOT_FOUND);
        }

        return json($product->export());
    }

    function api_product_find_by_id_json()
    {
        if ($_SESSION['CurrentUser_ID'] == null)
        {
            halt(NOT_FOUND);
        }

        $product = R::load('product', params('id'));
        if (R::$adapter->getAffectedRows() < 1)
        {
            halt(NOT_FOUND);
        }

        return json($product->export());
    }

    function api_stats_stock_html() {
        Security_Authorize();

        $deliveries = array();
        $pickups = array();

        $stock_changes = R::$f->begin()
            ->select('*, SUM(amount)')->as('amount')
            ->from('stock')
                ->join('product on product.id = stock.product_id')
                ->join('log on log.object = stock.id')
            ->where('(iscanceled = 0 or iscanceled is null)')
                ->and('action = \'created\'')
                ->and('object_type = \'stock\'')
                ->and('date BETWEEN STR_TO_DATE(?, \'%d-%b-%Y\') and DATE_ADD(STR_TO_DATE(?, \'%d-%b-%Y\'), INTERVAL 1 DAY) ')
                    ->put(params('start'))->put(params('end'))
            ->group_by('product_id, type')
            ->order_by('name asc')
            ->get();

        foreach($stock_changes as $stock)
        {
            if($stock['type'] == 'delivery') {
                $deliveries[] = $stock;
            } else {
                $pickups[] = $stock;
            }
        }

        $deliveries_count = 0;
        $delivery_data = PHP_EOL;
        $deliveries_table = '';
        foreach($deliveries as $delivery) {
            $deliveries_table .=
                '<tr>'.
                    '<td>' . $delivery['name'] . '</td>'.
                    '<td>&plus; ' . abs($delivery['amount']) . '</td>'.
                '</tr>';
            $deliveries_count += $delivery['amount'];
            $delivery_data .= '[\'' . $delivery['name'] . '\', ' . $delivery['amount'] . '],' . PHP_EOL;
        }

        $pickups_count = 0;
        $pickup_data = PHP_EOL;
        $pickups_table = '';
        foreach($pickups as $pickup) {
            $pickups_table .=
                '<tr>'.
                    '<td>' . $pickup['name'] . '</td>'.
                    '<td>&minus; ' . abs($pickup['amount']) . '</td>'.
                '</tr>';
            $pickups_count += abs($pickup['amount']);
            $pickup_data .= '[\'' . $pickup['name'] . '\', ' . abs($pickup['amount']) . '],' . PHP_EOL;
        }

        return partial('api/stock_stats_html.php', array(
            'deliveries_count' => $deliveries_count,
            'delivery_data' => $delivery_data,
            'deliveries_table' => $deliveries_table,

            'pickups_count' => $pickups_count,
            'pickup_data' => $pickup_data,
            'pickups_table' => $pickups_table,
        ));
    }

    function api_stats_order_html() {
        Security_Authorize();

        $orders = array();

        $orders = R::$f->begin()
            ->select('*, SUM(amount)')->as('amount')
            ->from('`order`')
                ->join('product on product.id = `order`.product_id')
                ->join('log on log.object = order.id')
            ->where('(iscanceled = 0 or iscanceled is null)')
                ->and('action = \'created\'')
                ->and('object_type = \'order\'')
                ->and('date BETWEEN STR_TO_DATE(?, \'%d-%b-%Y\') and DATE_ADD(STR_TO_DATE(?, \'%d-%b-%Y\'), INTERVAL 1 DAY) ')
                    ->put(params('start'))->put(params('end'))
            ->group_by('product_id')
            ->order_by('name asc')
            ->get();

        $orders_count = 0;
        $orders_data = PHP_EOL;
        $orders_table = '';
        foreach($orders as $order) {
            $orders_table .=
                '<tr>'.
                    '<td>' . $order['name'] . '</td>'.
                    '<td>&plus; ' . abs($order['amount']) . '</td>'.
                '</tr>';
            $orders_count += $order['amount'];
            $orders_data .= '[\'' . $order['name'] . '\', ' . $order['amount'] . '],' . PHP_EOL;
        }

        return partial('api/order_stats_html.php', array(
            'orders_count' => $orders_count,
            'orders_data' => $orders_data,
            'orders_table' => $orders_table,
        ));
    }

?>
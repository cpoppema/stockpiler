<?php

    require_once "library/limonade.php";
    // require_once "library/lightopenid/openid.php";

    require_once "library/redbean/rb.php";
    require("library/utils.php");
    require("config/config.php");
    require("library/hashing.php");
    require("library/security.php");

    /* Establish database connection */
    R::setup('mysql:host=' . Server . ';dbname=' . Database, Username, Password);
    // R::freeze();
    // R::debug(true);

    /* Modify configuration settings */
    function configure()
    {
        option('base_uri', '/');  # '/' or same as the RewriteBase or ENV:FRB in your .htaccess
        option('public_dir', 'public/');
        option('views_dir', 'views/');
        option('controllers_dir', 'controllers/');
    }

    /* Declare default layout page */
    function before()
    {
        layout('layout.php');
    }

    /* Declare default error page */
    function server_error($errno, $errstr, $errfile=null, $errline=null)
    {
        $args = compact('errno', 'errstr', 'errfile', 'errline');
        return html("error/error.php", "layout.php", $args);
    }

    /* Declare Common routes */
    dispatch('/', 'common_dashboard');

    /* Declare Security routes */
    dispatch('/login', 'login');
    dispatch_post('/login', 'login_post');
    dispatch('/login/reset', 'login_reset');
    dispatch_post('/login/reset', 'login_reset_post');
    // dispatch('/login/openid/google', 'login_openid_google');
    // dispatch_post('/login/openid/google', 'login_openid_google_post');
    // dispatch('/login/openid/remove', 'login_openid_remove');
    dispatch('/logout', 'logout');

    /* Declare Stock routes */
    dispatch('/stock', 'stock_history');
    dispatch('/stock/delivery', 'stock_delivery');
    dispatch_post('/stock/delivery', 'stock_delivery_post');
    dispatch('/stock/delivery/bulk', 'stock_bulk_delivery');
    dispatch_post('/stock/delivery/bulk', 'stock_delivery_bulk_post');
    dispatch('/stock/pickup/bulk', 'stock_bulk_pickup');
    dispatch_post('/stock/pickup/bulk', 'stock_pickup_bulk_post');
    dispatch('/stock/pickup', 'stock_pickup');
    dispatch_post('/stock/pickup', 'stock_pickup_post');
    dispatch('/stock/:id', 'stock_edit');
    dispatch_post('/stock/:id/edit', 'stock_edit_post');

    /* Declare Orders routes */
    dispatch('/orders', 'orders_history');
    dispatch('/orders/add', 'orders_add');
    dispatch_post('/orders/add', 'orders_add_post');
    dispatch('/orders/:id', 'orders_edit');
    dispatch_post('/orders/:id/edit', 'orders_edit_post');

    /* Declare Products routes */
    dispatch('/products', 'products_list');
    dispatch('/products/add', 'products_add');
    dispatch_post('/products/add', 'products_add_post');
    dispatch('/products/:id', 'products_edit');
    dispatch_post('/products/:id/edit', 'products_edit_post');

    /* Declare Users routes */
    dispatch('/users', 'users_list');
    dispatch('/users/add', 'users_add');
    dispatch_post('/users/add', 'users_add_post');
    dispatch('/users/:id', 'users_edit');
    dispatch_post('/users/:id/edit', 'users_edit_post');
    dispatch('/users/:id/delete', 'users_delete');
    dispatch('/users/:id/recover', 'users_recover');

    /* Look up an audit trail */
    dispatch('/audit', 'audit_trail');
    dispatch('/audit/product/:id', 'product_audit_trail');
    dispatch('/audit/user/:id', 'user_audit_trail');

    /* cron_stock_status */
    dispatch('/cron/email', 'cron_stock_status');

    /* Declare History routes */
    dispatch('/versions', 'versions');

	/* Declare API routes */
    dispatch('/api/table/products/:hash', 'api_product_table');
    dispatch('/api/json/product/find/code/:code', 'api_product_find_by_code_json');
    dispatch('/api/json/product/find/id/:id', 'api_product_find_by_id_json');
    dispatch('/api/html/stats/order/:start/:end', 'api_stats_order_html');
    dispatch('/api/html/stats/stock/:start/:end', 'api_stats_stock_html');

    run();

    R::close();

?>
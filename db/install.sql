-- This SQL is for Stockpiler specific


-- RANDOM USER WITH PASSWORD 'admin'
INSERT INTO  `user` (
	`id` ,
	`name` ,
	`username` ,
	`password` ,
	`email` ,
	`isadministrator` ,
	`createddate`
)
VALUES (
	NULL ,  'First name Last name',  'username',  '21232f297a57a5a743894a0e4a801fc3',  'your-email@example.com',  '1',  '2012-10-15 21:43:57'
);

ALTER TABLE  `user` ADD  `receivewarningemails` BOOL NOT NULL DEFAULT  '0' AFTER  `isadministrator`;
ALTER TABLE  `user` ADD  `hideinactive` BOOL NOT NULL DEFAULT  '0' AFTER  `receivewarningemails`;
ALTER TABLE  `user` ADD  `bulkisdefault` BOOL NOT NULL DEFAULT  '0' AFTER `hideinactive`;
ALTER TABLE  `user` ADD  `isreadonly` BOOL NOT NULL DEFAULT  '0' AFTER  `isadministrator`;
ALTER TABLE  `user` ADD  `hidepagedescription` BOOL NOT NULL DEFAULT  '0' AFTER `bulkisdefault`;
ALTER TABLE  `user` ADD  `isdeleted` BOOL NOT NULL DEFAULT  '0' AFTER `createddate`;

-- PRODUCTS STRUCTURE
CREATE TABLE  `products_product` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`name` VARCHAR( 200 ) NOT NULL ,
	`code` VARCHAR( 100 ) NOT NULL ,
	`minimumstock` INT NOT NULL ,
	`isdeleted` BOOL NOT NULL DEFAULT  '0'
) ENGINE = MYISAM ;

-- STOCK INFORMATION
CREATE TABLE  `products_stock` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`product_id` INT NOT NULL ,
	`type` ENUM(  'delivery',  'pickup' ) NOT NULL ,
	`amount` INT NOT NULL ,
	`iscanceled` BOOL NOT NULL DEFAULT  '0'
	`reason` TEXT NULL ,
) ENGINE = MYISAM ;

ALTER TABLE  `products_stock` ADD INDEX  `cancelled_index` (  `cancelled` )

CREATE TABLE  `products_order` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`product_id` INT NOT NULL ,
	`amount` INT NOT NULL ,
	`hasarrived` BOOL NOT NULL DEFAULT  '0',
	`iscanceled` BOOL NOT NULL DEFAULT  '0',
	`reason` INT NULL DEFAULT NULL
) ENGINE = MYISAM ;

ALTER TABLE  `products_order` ADD INDEX  `cancelled_index` (  `cancelled` )

-- AUDIT LOG
CREATE TABLE  `log` (
	`user_id` INT NOT NULL ,
	`action` ENUM(  'created',  'modified' ) NOT NULL ,
	`foreign_type` ENUM(  'product',  'stock', 'order' ) NOT NULL ,
	`foreign_id` INT NOT NULL ,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = MYISAM ;

ALTER TABLE log ENGINE = InnoDB;
ALTER TABLE products_order ENGINE = InnoDB;
ALTER TABLE products_product ENGINE = InnoDB;
ALTER TABLE products_stock ENGINE = InnoDB;
ALTER TABLE user ENGINE = InnoDB;

ALTER TABLE `log` CHANGE `foreign_type` `object_type` ENUM( 'product', 'stock', 'order' ) NOT NULL ,
CHANGE `foreign_id` `object` INT( 11 ) NOT NULL
ALTER TABLE `log` ADD `id` INT ( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST ;

<?php

    function Security_Login_Old_Style($username, $password)
    {
        $user_old_style = R::findOne('user', 'username = ? and password = ? and (isdeleted = 0 or isdeleted = null)', array($username, md5($password)));
        return $user_old_style;
    }

    function Security_Login($username, $password, $rememberme = false)
    {
        require Root . '/library/phpass-0.3/PasswordHash.php';

        // try login the new way
        $user = R::findOne('user', 'username = ? and (isdeleted = 0 or isdeleted = null)', array($username));
        if (!$user->hash)
        {
            // check if username + password exists using md5
            $user_old_style = Security_Login_Old_Style($username, $password);
            if(!$user_old_style->id)
            {
                return false;
            }

            // convert password to the new hashing mechanism
            $convert_hasher = new PasswordHash(8, FALSE);
            $new_hash = $convert_hasher->HashPassword($password);
            $user->hash = True;
            $user->password = $new_hash;
            R::store($user);
        } else {
            // verify new hashed password
            $hasher = new PasswordHash(8, FALSE);
            if (!$hasher->CheckPassword($password, $user->password))
            {
                return false;
            }
        }

        Security_Refresh($user);

        if ($rememberme == true)
        {
            setcookie("username", $user->username, time() + (60 * 60 * 24 * 7), "/", $_SERVER['HTTP_HOST'], true);
            setcookie("password", $user->password, time() + (60 * 60 * 24 * 7), "/", $_SERVER['HTTP_HOST'], true);
        }

        return true;
    }

    function Security_CookieLogin_Old_Style($username, $password)
    {
        $user_old_style = R::findOne('user', 'username = ? and password = ? and (isdeleted = 0 or isdeleted = null)', array($username, $password));
        return $user_old_style;
    }

    function Security_CookieLogin($username, $password)
    {
        require Root . '/library/phpass-0.3/PasswordHash.php';

        // try login the new way
        $user = R::findOne('user', 'username = ? and (isdeleted = 0 or isdeleted = null)', array($username));
        if (!$user->hash)
        {
            // check if username + password exists using md5
            $user_old_style = Security_CookieLogin_Old_Style($username, $password);
            if(!$user_old_style->id)
            {
                return false;
            }

            // convert password to the new hashing mechanism
            $convert_hasher = new PasswordHash(8, FALSE);
            $new_hash = $convert_hasher->HashPassword($password);
            $user->hash = True;
            $user->password = $new_hash;
            R::store($user);
        } else {
            // verify new hashed password
            $hasher = new PasswordHash(8, FALSE);
            if (!$hasher->CheckPassword($password, $user->password))
            {
                return false;
            }
        }

        Security_Refresh($user);

        setcookie("username", $user->username, time() + (60 * 60 * 24 * 7), "/", $_SERVER['HTTP_HOST'], true);
        setcookie("password", $user->password, time() + (60 * 60 * 24 * 7), "/", $_SERVER['HTTP_HOST'], true);

        return true;
    }

    function Security_Logout()
    {
        session_destroy();

        setcookie("username", "", time() - (60 * 60), "/", $_SERVER['HTTP_HOST']);
        setcookie("password", "", time() - (60 * 60), "/", $_SERVER['HTTP_HOST']);

        return true;
    }

    function Security_Authorize()
    {
        if ($_SESSION['CurrentUser_ID'] == null)
        {
            header("Location: " . option('base_uri') . "login");
            exit;
        }
    }

    function Security_Refresh($user)
    {
        if ($user->id)
        {
            $_SESSION['CurrentUser_ID'] = $user->id;
            $_SESSION['CurrentUser_Name'] = $user->name;
            $_SESSION['CurrentUser_Username'] = $user->username;
            $_SESSION['CurrentUser_IsAdministrator'] = $user->isadministrator;
            $_SESSION['CurrentUser_IsReadOnly'] = $user->isreadonly;
            $_SESSION['CurrentUser_ReceiveWarningEmails'] = $user->receivewarningemails;
            $_SESSION['CurrentUser_HideInacitve'] = $user->hideinactive;
            $_SESSION['CurrentUser_BulkIsDefault'] = $user->bulkisdefault;
            $_SESSION['CurrentUser_ManualModeIsDefault'] = $user->manualmodeisdefault;
            $_SESSION['CurrentUser_HidePageDescription'] = $user->hidepagedescription;
        }
    }

?>
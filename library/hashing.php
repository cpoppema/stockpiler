<?php

    function is_valid_api_hash($controller_function, $hash)
    {
        global $controller_hashes;

        // check controller specific hash first
        if (isset($controller_hashes))
        {
            if (array_key_exists($controller_function, $controller_hashes))
            {
                return $controller_hashes[$controller_function] == $hash;
            }
        }

        // check global hash,
        if (defined('Hash'))
        {
            if (strlen(trim(Hash)) == 0) // allow if defined but empty
            {
                return true;
            } else { // compare otherwise
                return Hash == $hash;
            }
        }

        return false; // not allowed, check seems neccesary but configuration is missing
    }

?>